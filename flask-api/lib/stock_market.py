import requests



def get_market_details():
    ret_list = dict()
    blist = dict()
    flist = dict()
    url_b = 'https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_bourse'
    url_f = 'https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_farabourse'
    breq = requests.get(url_b).json()
    freq = requests.get(url_f).json()
    ret_list['bourse'] = breq['bourse']
    ret_list['farabourse'] = freq['fara-bourse']
    return ret_list
