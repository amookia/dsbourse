import base64
import uuid
import re

def base64_validation(text):
    try:
        validate_formats = ['image/png','image/jpeg','image/jpg']
        image_type = re.findall('data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*',text)[0]
        if image_type not in validate_formats:
            raise Exception('ERROR INVALID FORMAT')
        base64_image = text.split(',')
        base64.b64decode(base64_image[1])
        return True
    except Exception as e:
        return False


def base64_save(image):
    base64_image = image.split(',')[1]
    image_type = re.findall('data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*',image)[0]
    format = image_type.replace('image/','')
    images_dir = './media/wallstreet/'
    filename = str(uuid.uuid4().hex[:20].lower()) + '.' + format
    with open(images_dir + filename,"wb") as f:
        f.write(base64.b64decode(base64_image))

    return filename
