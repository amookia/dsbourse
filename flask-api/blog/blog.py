from flask import ( Blueprint , jsonify )
from mongo import db
import pymongo
from bson.objectid import ObjectId
import config
import jdatetime

blog_api = Blueprint('blog',__name__,url_prefix='/api/blog')



@blog_api.route('/index',methods=['GET'])
def index_blog():
    posts = db.posts.find().sort([("created_at", pymongo.DESCENDING)])[:8]
    count = db.posts.count_documents({})
    post_lists = list()
    for post in posts:
        id = str(post['_id'])
        head_image = config.domain + config.static_media + '/blog/' + str(post['head_image'])
        title = str(post['title'])
        description = str(post['description'])
        post_lists.append({
        'id':id , 'head_image' : head_image , 'title' : title , 'description' : description
        })

    return jsonify({'posts':post_lists,'total':count})



@blog_api.route('/page/<int:page>',methods=['GET'])
def pagination_blog(page):
    frompost = ( int(page) - 1 ) * 8
    topost = int(page) * 8
    posts = db.posts.find().sort([("created_at", pymongo.DESCENDING)])[frompost:topost]
    post_lists = list()
    for post in posts:
        id = str(post['_id'])
        head_image = config.domain + config.static_media + '/blog/' + str(post['head_image'])
        title = str(post['title'])
        description = str(post['description'])
        post_lists.append({
        'id':id , 'head_image' : head_image , 'title' : title , 'description' : description
        })
    if len(post_lists) == 0:
        message = 'پستی یافت نشد'
        return jsonify({'error': message }),404

    return jsonify({'posts':post_lists})



@blog_api.route('/post/<path:id>')
def visit_post(id):
    try:
        id = str(id)
        post = db.posts.find_one({'_id': ObjectId(id) })
        head_image = config.domain + config.static_media + '/blog/' + str(post['head_image'])
        title = str(post['title'])
        description = str(post['description'])
        body = str(post['body'])
        date = post['created_at'].timestamp()
        date = jdatetime.datetime.fromtimestamp(date).strftime('%Y/%m/%d %H:%M')
        resp = {'id':id , 'head_image' : head_image , 'title' : title ,
        'description' : description , 'body': body , 'date' : date}
        return jsonify(resp)
    except:
        message = 'پست یافت نشد'
        return jsonify({'error': message }),404
