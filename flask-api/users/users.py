from flask import (Blueprint , jsonify , request)
from .forms import (UsersRegister , UserRegisterVerify , UserForgetPass1 , UserForgetPass2 ,
    UsersLogin , UsersEditProfile , UsersUpdateAvatar)
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity,verify_jwt_in_request,get_jwt_claims)
from mongo import db
import config

users_api = Blueprint('users',__name__,url_prefix='/api/users')


jwt = JWTManager()

@users_api.route('/register',methods=['POST'])
def users_register():
    form = request.get_json() if request.is_json else {}
    try:
        UsersRegister(**form)
        return jsonify({'message':'کد ارسال شد'})

    except Exception as e:
        return jsonify({'error':str(e)}),400




@users_api.route('/register/verify',methods=['POST'])
def users_register_verify():
    form = request.get_json() if request.is_json else {}
    try:
        UserRegisterVerify(**form)
        phone_number = form['phone_number']
        access_token = create_access_token(identity=phone_number,expires_delta=False)
        return jsonify({'token':access_token})
    except Exception as e:
        return jsonify({'error':str(e)}),400


@users_api.route('/forget/s1',methods=['POST'])
def users_forget_pass1():
    form = request.get_json() if request.is_json else {}
    try:
        UserForgetPass1(**form)
        return jsonify({'message':'کد ارسال شد'})
    except Exception as e:
        return jsonify({'error':str(e)}),400


@users_api.route('/forget/s2',methods=['POST'])
def users_forget_pass2():
    form = request.get_json() if request.is_json else {}
    try:
        UserForgetPass2(**form)
        return jsonify({'message':'پسورد با موفقیت تغییر کرد'})
    except Exception as e:
        return jsonify({'error':str(e)}),400



@users_api.route('/login',methods=['POST'])
def users_login():
    form = request.get_json() if request.is_json else {}
    try:
        phone_number = UsersLogin(**form)
        access_token = create_access_token(identity=str(phone_number),expires_delta=False)
        return jsonify({'token':access_token})
    except Exception as e:
        return jsonify({'error':str(e)}),200


@users_api.route('/me',methods=['GET'])
@jwt_required
def users_me():
    current_user = get_jwt_identity()
    me = db.users.find_one({'phone_number':current_user})
    avatar = config.domain + config.static_media + '/avatar/' + me['avatar']
    retlist = {'name':me['name'],'username':me['username'],
    'phone_number':me['phone_number'],'avatar':avatar}
    return jsonify(retlist)


@users_api.route('/editprofile',methods=['PATCH'])
@jwt_required
def edit_profile():
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        UsersEditProfile(current_user,**form)
        return jsonify({'message':'پروفایل با موفقیت آپدیت شد'})
    except Exception as e:
        return jsonify({'error':str(e)}),400


@users_api.route('/editprofile/avatar',methods=['POST'])
@jwt_required
def update_avatar():
    try:
        current_user = get_jwt_identity()
        filename = UsersUpdateAvatar(current_user,**request.files)
        return jsonify({'message':'آواتار با موفقیت آپدیت شد'})
    except Exception as e:
        return jsonify({'error':str(e)}),400
