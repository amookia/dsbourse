from mongo import db
from lib.send_sms import send
from random import randint
from datetime import datetime
from flask_bcrypt import Bcrypt
import re
from werkzeug.utils import secure_filename
import os
import uuid

bcrypt = Bcrypt()

class Messages:
    INVALID_FORM = 'ورودی ها دوباره چک شود'
    ERR_NAME = 'اسم نامعتبر'
    ERR_USERNAME = 'نام کاربری نامعتبر میباشد'
    ERR_PASSWORD = 'پسورد باید بزرگتر از 8 کارکتر باشد'
    ERR_NUMBER = 'شماره تلفن نامعتبر میباشد'
    ERR_SMSOTP = 'کد برای این شماره ارسال شده است'
    INVALID_CODE = 'کد نا معتبر'
    ERR_USER_TAKEN = 'متاسفانه نام کاربری تکراری میباشد'
    ERR_USER_NOTFOUND = 'کاربری با این شماره یافت نشد'
    ERR_LOGIN = 'نام کاربری یا رمز عبور اشتباه میباشد'
    ERR_FILE_FORMAT = 'فرمت عکس نامعتبر میباشد'
    ERR_FILE_LENGTH = 'حجم عکس نباید بیشتر از {} باشد'


class UsersRegister:
    def __init__(self,**form):
        user_regex = '^[a-zA-Z0-9]+$'
        if all (i in form for i in ('name','username','password','phone_number')):
            name = str(form['name'])
            username = str(form['username']).lower()
            password = str(form['password'])
            phone_number = str(form['phone_number'])

            if len(name) < 3 :
                raise Exception(Messages.ERR_NAME)

            elif len(username) < 4 or username.isdigit() :
                raise Exception(Messages.ERR_USERNAME)

            elif re.match("^[a-zA-Z0-9]+$", username) is None:
                raise Exception(Messages.ERR_USERNAME)

            elif len(password) < 8 :
                raise Exception(Messages.ERR_PASSWORD)

            elif len(phone_number) < 10 or len(phone_number) > 11 or phone_number[0] != "0" or phone_number.isdigit() != True:
                raise Exception(Messages.ERR_NUMBER)
            check_username = db.users.count({'username':username}) != 0
            if check_username:
                raise Exception(Messages.ERR_USER_TAKEN)
            check_phone = db.otp.count({'phone_number':phone_number}) == 0 and db.users.count({'phone_number':phone_number}) == 0
            if check_phone :
                random_num = randint(100000,999999)
                db.otp.insert_one({
                'type':'register','phone_number':phone_number,'name':name,
                'username':username,'password':password,'code':random_num
                ,'expire':datetime.utcnow()})
                #send(phone_number[1:],random_num)
            else:
                raise Exception(Messages.ERR_SMSOTP)

        else:
            raise Exception(Messages.INVALID_FORM)



class UserRegisterVerify:
    def __init__(self,**form):
        phone_number = str(form['phone_number'])
        code = str(form['code'])
        if 'phone_number' in form and 'code' in form:
            if len(phone_number) < 10 or len(phone_number) > 11 or phone_number[0] != "0" or phone_number.isdigit() != True:
                raise Exception(Messages.ERR_NUMBER)

            otp_find = db.otp.find_one({'type':'register','phone_number':phone_number})
            if otp_find is not None:
                otp_code = otp_find['code']
                if str(otp_code) == code:
                    name = otp_find['name']
                    username = otp_find['username']
                    password = bcrypt.generate_password_hash(otp_find['password'])
                    db.users.insert_one({'name':name,'username':username,'phone_number':phone_number,'avatar':'default.png',
                    'password':password,'created_at':datetime.utcnow()})
                    db.otp.remove({'type':'register','phone_number':phone_number})
                else:
                    raise Exception(Messages.INVALID_CODE)
            else:
                raise Exception(Messages.INVALID_CODE)
        else:
            raise Exception(Messages.INVALID_FORM)


class UserForgetPass1:
    def __init__(self,**form):
        if 'phone_number' in form:
            phone_number = str(form['phone_number'])
            if len(phone_number) < 10 or len(phone_number) > 11 or phone_number[0] != "0" or phone_number.isdigit() != True:
                raise Exception(Messages.ERR_NUMBER)
            user = db.users.find_one({'phone_number':phone_number})
            if user is not None:
                find_otp = db.otp.count({'type':'forget','phone_number':phone_number}) != 0
                if find_otp:
                    raise Exception(Messages.ERR_SMSOTP)
                else:
                    random_num = randint(100000,999999)
                    print(random_num)
                    db.otp.insert_one({'type':'forget','phone_number':phone_number,'code':random_num})
                    send(phone_number[1:],random_num)
            else:
                raise Exception(Messages.ERR_USER_NOTFOUND)
        else:
            raise Exception(Messages.INVALID_FORM)


class UserForgetPass2:
    def __init__(self,**form):
        if all (i in form for i in ('new_password','code','phone_number')):
            phone_number = str(form['phone_number'])
            new_password = bcrypt.generate_password_hash(str(form['new_password']))
            code = str(form['code'])
            if len(phone_number) < 10 or len(phone_number) > 11 or phone_number[0] != "0" or phone_number.isdigit() != True:
                raise Exception(Messages.ERR_NUMBER)

            elif len(new_password) < 8 :
                raise Exception(Messages.ERR_PASSWORD)

            otp = db.otp.find_one({'type':'forget','phone_number':phone_number})
            if otp is not None:
                otp_code = str(otp['code'])
                if otp_code == code:
                    db.users.update_one({'phone_number':phone_number},{'$set':{'password':new_password}})
                    db.otp.remove({'type':'forget','phone_number':phone_number})
                else:
                    raise Exception(Messages.INVALID_CODE)
            else:
                raise Exception(Messages.INVALID_CODE)


class UsersLogin:
    def __init__(self,**form):
        if all (i in form for i in ('username','password')):
            username = str(form['username'])
            password = str(form['password'])
            if len(username) != 0 or len(password) != 0:
                user = db.users.find_one({'username':username})
                if user is not None:
                    self.phone_number = user['phone_number']
                    if bcrypt.check_password_hash(user['password'],password) != True:
                        raise Exception(Messages.ERR_LOGIN)
                else:
                    raise Exception(Messages.ERR_LOGIN)
            else:
                raise Exception(Messages.ERR_LOGIN)

    def __str__(self):
        return self.phone_number


class UsersEditProfile:
    def __init__(self,phone_number,**form):
        update_form = {}
        if 'username' in form:
            username = str(form['username'])
            if re.match("^[a-zA-Z0-9]+$", username) is None:
                raise Exception(Messages.ERR_USERNAME)

            username_nexists = db.users.count({'username':username}) == 0
            if username_nexists :
                update_form['username'] = username
            else:
                raise Exception(Messages.ERR_USER_TAKEN)
        if 'name' in form:
            name = str(form['name'])
            if len(name) < 2 or len(name) > 35:
                raise Exception(Messages.ERR_NAME)
            update_form['name'] = name

        db.users.update_one({'phone_number':phone_number},{'$set':update_form})



class UsersUpdateAvatar:
    def __init__(self,user,**form):
        max_length = 600
        formats = ['jpeg','png','jpg']
        if 'file' in form:
            image = form['file']
            if image.filename == '':
                raise Exception(Messages.ERR_FILE_FORMAT)


            filename = secure_filename(image.filename)
            format = image.filename.split('.')[-1:]
            if format[0].lower() not in formats:
                raise Exception(Messages.ERR_FILE_FORMAT)
            image.seek(0, os.SEEK_END)
            image_size = image.tell()
            if image_size > 600000:
                raise Exception(Messages.ERR_FILE_LENGTH.format(max_length))

            user_info = db.users.find_one({'phone_number':user})
            filename = str(user_info['_id']) + '.' + str(format[0]).lower()
            image.seek(0)
            image.save(os.path.join('./media/avatar', filename))
            db.users.update_one({'phone_number':user},{'$set':{'avatar':filename}})


        else:
            raise Exception(Messages.ERR_FILE_FORMAT)
