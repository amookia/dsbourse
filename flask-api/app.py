from flask import Flask
from flask_jwt_extended import JWTManager
from flask_cors import CORS
import config

app = Flask(__name__, static_folder="./media",static_url_path=config.static_media)

app.config['SECRET_KEY'] = config.secret
app.config['JWT_SECRET_KEY'] = config.secret
app.config['JWT_HEADER_TYPE'] = ''
#app.config['MAX_CONTENT_PATH'] =



jwt = JWTManager(app)
CORS(app)

#register blueprint
from admin.admin import admin_api
app.register_blueprint(admin_api)      #admin
from blog.blog import blog_api
app.register_blueprint(blog_api)       #blog
from users.users import users_api
app.register_blueprint(users_api)      #users
from wallstreet.wallstreet import wallstreet_api
app.register_blueprint(wallstreet_api) #wallstreet_api


if __name__ == '__main__':
    app.run(port=5000,debug=True)
