from pymongo import MongoClient
import config

class db:
    def __init__(self):
        client = MongoClient(config.mongouri)
        db = client['dsbourse']
        self.admin = db['admin']
        self.posts = db['posts']
        self.users = db['users']
        self.wallstreet = db['wallstreet']
        self.otp = db['otp']
        self.otp.ensure_index('expire', expireAfterSeconds=2*60)

db = db()
