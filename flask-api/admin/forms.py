from mongo import db
from flask_bcrypt import Bcrypt
from werkzeug.utils import secure_filename
import uuid
import os
from datetime import datetime
from bson.objectid import ObjectId

bcrypt = Bcrypt()

class Errors:
    LOGIN_FAILED = 'یوزرنیم یا پسورد اشتباه میباشد'
    INVALID_FORM = 'ورودی ها دوباره چک شود'
    INVALID_PASS = 'پسورد اشتباه میباشد'
    INVALID_NEWPASS = 'پسورد نباید کوچکتر از 8 کارکتر باشد'
    INVALID_FORMAT = 'فرمت فایل نامعتبر میباشد'
    SUCCESS_EDIT = 'پست با موفقیت تغییر یافت'
    INVALID_ADMIN = 'نام کاربری تکراری میباشد'

class LoginAdmin:
    def __init__(self,**form):
        if 'username' in form and 'password' in form:
            admin = db.admin.find_one({'username':form['username']})
            if admin :
                if form['username'] == admin['username'] and bcrypt.check_password_hash(admin['password'], form['password']):
                    pass
                else:
                    raise Exception(Errors.LOGIN_FAILED)
            else:
                raise Exception(Errors.LOGIN_FAILED)

        else:
            raise Exception('ERROR')


class ChangepwAdmin:
    def __init__(self,username,**form):
        if 'oldpass' in form and 'newpass' in form:
            oldpass = str(form['oldpass'])
            newpass = str(form['newpass'])
            admin = db.admin.find_one({'username':username})
            if  bcrypt.check_password_hash(admin['password'], oldpass) != True:
                raise Exception(Errors.INVALID_PASS)
            elif len(newpass) < 8 :
                raise Exception(Errors.INVALID_NEWPASS)
            db.admin.update_one({'username':username},{'$set':{'password':bcrypt.generate_password_hash(newpass)}})
        else:
            raise Exception(form.INVALID_FORM)



class UploadAdmin:
    def __init__(self,**form):
        formats = ['image/jpeg','image/png']
        if 'file' in form:
            image = form['file']
            if image.content_type not in formats:
                raise Exception(Errors.INVALID_FORMAT)
            elif image.filename == '':
                raise Exception(Errors.INVALID_FORMAT)

            filename = secure_filename(image.filename)
            format = filename.split('.')[-1:]
            self.filename = str(uuid.uuid4().hex[:10].lower()) + '.' + str(format[0])
            image.save(os.path.join('./media/blog', self.filename))
    def __str__(self):
        return self.filename


class CreatePostAdmin:
    def __init__(self,author,**form):
        if 'head_image' in form and 'description' in form and 'body' in form and 'title' in form:
            head_image  = str(form['head_image'])
            description = str(form['description'])
            body = str(form['body'])
            title = str(form['title'])
            if len(head_image) == 0 or len(description) ==0 or len(body) == 0 or len(title) == 0:
                raise Exception(Errors.INVALID_FORM)

            self.post = db.posts.insert_one({'head_image':head_image,'description':description,'title':title,
            'body':body,'author':author,'created_at':datetime.now()})
        def __str__(self):
            return self.post


class EditPostAdmin:
    def __init__(self,id,**form):
        self.id = id
        valid_form = dict()
        if 'head_image' in form:
            valid_form['head_image'] = str(form['head_image'])
        if 'description' in form:
            valid_form['description'] = str(form['description'])
        if 'body' in form:
            valid_form['body'] = str(form['body'])
        if 'title' in form:
            valid_form['title'] = str(form['title'])

        if len(valid_form) == 0:
            raise Exception(Errors.INVALID_FORM)

        print(valid_form)
        db.posts.update_one({'_id': ObjectId(id) },{'$set': valid_form })

    def __str__(self):
        return Errors.SUCCESS_EDIT

class CreateNewAdmin:
    def __init__(self,**form):
        if 'username' in form and 'password' in form:
            username = str(form['username'])
            password = str(form['password'])
            if len(password) < 8 :
                raise Exception(Errors.INVALID_NEWPASS)

            if db.admin.count({'username' : username }) != 0 or username == '':
                raise Exception(Errors.INVALID_ADMIN)

            db.admin.insert_one({'username':username,'password':bcrypt.generate_password_hash(password)})
