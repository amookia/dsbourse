from flask import ( Blueprint , jsonify , request )
from admin.forms import (LoginAdmin , ChangepwAdmin , UploadAdmin , CreatePostAdmin , EditPostAdmin , CreateNewAdmin)
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity,verify_jwt_in_request,get_jwt_claims)
from mongo import db
from functools import wraps
from bson.objectid import ObjectId
import jdatetime
import pymongo



def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['isadmin'] != True:
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper


admin_api = Blueprint('admin',__name__,url_prefix='/api/adminisgod')
jwt = JWTManager()




@admin_api.route('/login',methods=['POST'])
def login():
    form = request.get_json() if request.is_json else {}
    try:
        LoginAdmin(**form)
        access_token = create_access_token(identity=form['username'],user_claims={'isadmin':True},expires_delta=False)
        return jsonify({'token':access_token})

    except Exception as e:
        return jsonify({'error': str(e)})



@admin_api.route('/changepw',methods=['POST'])
@admin_required
def changepw():
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        ChangepwAdmin(current_user,**form)
        return jsonify({'message':'پسورد با موفقیت تغییر یافت'})
    except Exception as e:
        return jsonify({'error': str(e) }),400



@admin_api.route('/upload',methods=['POST'])
@admin_required
def upload_media():
    try:
        filename = UploadAdmin(**request.files)
        return jsonify({'filename':str(filename)})
    except Exception as e:
        return jsonify({'error':str(e)}),400



@admin_api.route('/post/create',methods=['POST'])
@admin_required
def create_post():
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        get_post = CreatePostAdmin(current_user,**form)
        return jsonify({'message':'پست با موفقیت ساخته شد','id':str(get_post.post.inserted_id)})
    except Exception as e:
        return jsonify({'error':str(e)}),400



@admin_api.route('/post/<path:id>',methods=['DELETE'])
@admin_required
def delete_post(id):
    delete = db.posts.delete_one({'_id': ObjectId(id)}).deleted_count
    if delete == 0:
        message = 'آیدی نامعتبر'
        return jsonify({'error':message}),400
    else:
        message = 'پست با موفقیت حذف شد'
        return jsonify({'message':message})



@admin_api.route('/post/<path:id>',methods=['PATCH'])
@admin_required
def edit_post(id):
    form = request.get_json() if request.is_json else {}
    find_post = db.posts.find_one({'_id': ObjectId(id)})
    if find_post:
        try:
            edit_post = EditPostAdmin(id,**form)
            #db.posts.update_one({'_id': ObjectId(id) },{'$set': edit_post })
            return jsonify({'message': 'پست با موفقیت تغییر یافت'})
        except Exception as e:
            return jsonify({'error':str(e)}),400
    else:
        message = 'آیدی نامعتبر'
        return jsonify({'error':message}),400



@admin_api.route('/post/list',methods=['GET'])
@admin_required
def post_list():
    all_posts = db.posts.find().sort([("created_at", pymongo.DESCENDING)])
    retlist = list()
    for post in all_posts:
        id = str(post['_id'])
        title = post['title']
        date =  post['created_at'].timestamp()
        date = jdatetime.datetime.fromtimestamp(date).strftime('%Y/%m/%d %H:%M')
        retlist.append({'id':id,'title':title,'date':date})
    if len(retlist) != 0:
        return jsonify(retlist)
    else:
        return jsonify({'error':'پستی برای نمایش وجود ندارد'})



@admin_api.route('/newadmin',methods=['POST'])
@admin_required
def create_new_admin():
    form = request.get_json() if request.is_json else {}
    try:
        CreateNewAdmin(**form)
        return jsonify({'message': 'ادمین جدید با موفقیت ساخته شد'})

    except Exception as e:
        return jsonify({'error': str(e) }),400



@admin_api.route('/me',methods=['GET'])
@admin_required
def admin_get_me():
    current_user = get_jwt_identity()
    return jsonify({'username':current_user})
