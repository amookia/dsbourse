from mongo import db
from lib.image_tweet import base64_validation , base64_save
from datetime import datetime
from bson.objectid import ObjectId
import re


class Messages:
    ERR_INVALID_FORM = 'ورودی ها دوباره چک شود'
    ERR_MAXIMUM_IMAGE = 'تعداد عکس ها نباید بیشتر از {} باشد'
    ERR_NOT_FOUND = 'NOT FOUND'

class TweetSend:
    def __init__(self,user,**form):
        MAX_IMAGES = 2
        valid_form = dict()
        files = list()
        user_info = db.users.find_one({'phone_number':user})

        if 'text' in form:
            text = str(form['text'])
            if len(text) == 0:
                raise Exception(Messages.ERR_INVALID_FORM)
            valid_form['text'] = str(form['text'])

        valid_form['images'] = []
        if 'image' in form:
            images = form['image']
            if len(images) > 2:
                raise Exception(Messages.ERR_MAXIMUM_IMAGE.format(MAX_IMAGES))
            #Check for valid base64
            for image in images:
                if base64_validation(image) != True:
                    raise Exception(Messages.ERR_INVALID_FORM)
            #Saving images
            for image in images:
                filename = base64_save(image)
                files.append(filename)
            valid_form['images'] = files

        valid_form['writer'] = user_info['username']
        valid_form['user_id'] = str(user_info['_id'])
        valid_form['likers'] = []
        valid_form['comments'] = []
        valid_form['created_at'] = datetime.now()
        self.id = db.wallstreet.insert_one(valid_form)


    def __str__(self):
        return str(self.id.inserted_id)



class TweetLike:
    def __init__(self,user,id,**form):
        if 'like' in form:
            user_id = db.users.find_one({'phone_number':user})
            like_it = None
            like = form['like']
            if str(type(like)) != '''<class 'bool'>''':
                raise Exception(Messages.ERR_INVALID_FORM)
            likers = db.wallstreet.find_one({'_id' : ObjectId(id)})
            if likers is None:
                raise Exception(Messages.ERR_NOT_FOUND)
            likers = likers['likers']
            if like == True :
                if str(user_id['_id']) not in likers:
                    likers.append(str(user_id['_id']))
            if like == False :
                if str(user_id['_id']) in likers:
                    likers.remove(str(user_id['_id']))

            print(likers)
            db.wallstreet.update_one({'_id': ObjectId(id) },{'$set':{'likers':likers}})



class TweetComment:
    def __init__(self,id,user,**form):
        files = list()
        tweet = db.wallstreet.find_one({'_id':ObjectId(id)})
        if tweet is None:
            raise Exception(Messages.ERR_NOT_FOUND)
        valid_form = dict()
        user = db.users.find_one({'phone_number':user})
        user_id = str(user['_id'])
        valid_form['user_id'] = user_id
        if 'text' in form:
            text = str(form['text'])
            if len(text) == 0:
                raise Exception(Messages.ERR_INVALID_FORM)
            valid_form['text'] = text

        if 'image' in form:
            images = form['image']
            if len(images) > 2:
                raise Exception(Messages.ERR_MAXIMUM_IMAGE.format(MAX_IMAGES))
            for image in images:
                filename = base64_save(image)
                files.append(filename)

        valid_form['images'] = files

        if len(valid_form) == 0:
            raise Exception(Messages.ERR_INVALID_FORM)

        db.wallstreet.update({'_id':ObjectId(id)},{'$push':{'comments':valid_form}})



class TweetSearchPrev:
    def __init__(self,**form):
        valid_form = dict()
        NOSPACE_REGEX = '^\S+$'
        if 'name' in form:
            name = str(form['name'])
            if len(name) == 0 or len(name) > 10:
                raise Exception(Messages.ERR_INVALID_FORM)
            if re.match(NOSPACE_REGEX, name) is None:
                raise Exception(Messages.ERR_INVALID_FORM)
