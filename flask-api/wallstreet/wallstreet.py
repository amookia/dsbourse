from flask import (Blueprint , jsonify , request)
from .forms import (TweetSend,TweetLike,TweetComment,TweetSearchPrev)
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity,jwt_optional)
from mongo import db
from bson.objectid import ObjectId
import config
from lib.stock_market import get_market_details
import pymongo

wallstreet_api = Blueprint('wallstreet',__name__,url_prefix='/api/wallstreet')


jwt = JWTManager()



@wallstreet_api.route('/tweet',methods=['POST'])
@jwt_required
def send_tweet():
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        id = TweetSend(current_user,**form)
        return jsonify({'message':'توییت با موفقیت ارسال شد','id':str(id)})
    except Exception as e:
        return jsonify({'error':str(e)}),400



@wallstreet_api.route('/tweet/<path:id>',methods=['GET'])
@jwt_optional
def visit_tweet(id):
    images = list()
    comments = list()
    current_user = get_jwt_identity()
    tweet = db.wallstreet.find_one({'_id' : ObjectId(id) })
    user_db = db.users.find_one({'_id': ObjectId(tweet['user_id'])})
    user_avatar = config.domain + config.static_media + '/avatar/' + user_db['avatar']
    if tweet is None:
        err = 'توییت مورد نظر پیدا نشد'
        return jsonify({'error': err }),404

    ret_list = dict()

    ret_list['liked'] = False
    if current_user is not None:
        user = db.users.find_one({'phone_number':current_user})
        if str(user['_id']) in tweet['likers']:
            ret_list['liked'] = True

    for cm in tweet['comments']:
        comment_images = list()
        cm_user = db.users.find_one({'_id':ObjectId(cm['user_id'])})
        cm_username = cm_user['username']
        cm_avatar = config.domain + config.static_media + '/wallstreet/' + cm_user['avatar']
        cm_text = cm['text']
        cm_text = cm['text']
        for image in cm['images']:
            image_url = config.domain + config.static_media + '/wallstreet/' + image
            comment_images.append(image_url)
        comments.append({
        'user_id':cm['user_id'],'username':cm_username,
        'avatar':cm_avatar,'text':cm['text'],'images':comment_images})

    ret_list['text'] = tweet['text']
    for image in tweet['images']:
        images.append(config.domain + config.static_media + '/wallstreet/' + image)

    ret_list['images'] = images
    ret_list['comments'] = comments
    ret_list['likes'] = len(tweet['likers'])
    ret_list['user'] = {'username':user_db['username'],'id':tweet['user_id'],'avatar':user_avatar}

    return jsonify(ret_list)



@wallstreet_api.route('/like/<path:id>',methods=['POST'])
@jwt_required
def like_tweet(id):
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        TweetLike(current_user,id,**form)
        return jsonify({'message':'DONE'})
    except Exception as e:
        return jsonify({'error' : str(e)}),400


@wallstreet_api.route('/index',methods=['GET'])
def wallstreet_index():
    MAXIMUM_COMMENT = 3
    tweets = list()
    latest_tweets = db.wallstreet.find().sort([("created_at", pymongo.DESCENDING)])[:10]
    for tweet in latest_tweets:
        comments = list()
        images_links = list()
        id = tweet['_id']
        text = tweet['text']
        likes = len(tweet['likers'])

        #comments section
        count = 0
        for cm in tweet['comments']:
            if count > MAXIMUM_COMMENT:
                break
            comment_images = list()
            cm_user = db.users.find_one({'_id':ObjectId(cm['user_id'])})
            cm_username = cm_user['username']
            cm_avatar = config.domain + config.static_media + '/avatar/' + cm_user['avatar']
            cm_text = cm['text']
            for image in cm['images']:
                image_url = config.domain + config.static_media + '/wallstreet/' + image
                comment_images.append(image_url)
            comments.append({
            'user_id':cm['user_id'],'username':cm_username,
            'avatar':cm_avatar,'text':cm['text'],'images':comment_images})
            count += 1

        #tweet section
        user_id = tweet['user_id']
        user_db = db.users.find_one({'_id' : ObjectId(user_id) })
        user_avatar = config.domain + config.static_media + '/avatar/' + user_db['avatar']
        user = user_db['username']
        images = tweet['images']
        for image in images:
            images_links.append(config.domain + config.static_media + '/wallstreet/' + image)

        #comments and tweet for specific id
        tweets.append({'id':str(id),'text':text,'likes':likes,'images':images_links,
        'comments':comments,'user':{'username':user,'id':user_id,'avatar':user_avatar}})

    return jsonify({'tweets':tweets})


@wallstreet_api.route('/market',methods=['GET'])
def market_index():
    market_det = get_market_details()
    return jsonify(market_det)


@wallstreet_api.route('/comment/<path:id>',methods=['POST'])
@jwt_required
def wallstreet_comment(id):
    form = request.get_json() if request.is_json else {}
    current_user = get_jwt_identity()
    try:
        TweetComment(id,current_user,**form)
        return jsonify({'message':'کامنت با موفقیت ارسال شد'})

    except Exception as e:
        return jsonify({'error':str(e)}),400


@wallstreet_api.route('/search/tag',methods=['POST'])
@jwt_required
def wallstreet_searchprev():
    form = request.get_json() if request.is_json else {}
    try:
        TweetSearchPrev(**form)
        result = list()
        name = '#' + str(form['name'])
        search = db.wallstreet.find({'text':{'$regex':name}})
        for res in search:
            print(res)
        return jsonify({'result':'result'})
    except Exception as e:
        return jsonify({'error':str(e)}),400
