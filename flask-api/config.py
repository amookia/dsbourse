from dotenv import load_dotenv
#load_dotenv()
import os

mongouri = os.getenv('MONGOURI')
secret = os.getenv('SECRET')
adminpass = os.getenv('ADMINPASS')
domain = os.getenv('DOMAIN')
kavenegar = os.getenv('KAVENEGAR')
static_media = '/media'
