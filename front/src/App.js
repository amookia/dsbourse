import React , { Component } from 'react'
import {  Route, Switch, withRouter, Redirect } from 'react-router-dom'

import Layout from './hoc/Layout/Layout'
import {connect} from 'react-redux'
import Home from './components/Home/Home'
import Blog from './components/Blog/Blog'
import Tweet from './components/Tweet/Tweet'
import * as actions from './store/actions/index'
import Login from './components/Login/Login'
import SignUp from './components/SignUp/SignUp'
import Dashboard from './components/Admin/Dashboard/Dashboard'
import Posts from './components/Admin/Dashboard/Posts/Posts'
import LoginAdmin from './components/Admin/Login/Login' 
import newPost from './components/Admin/Dashboard/NewPost/NewPost'
import Post from './components/Post/Post'
import editPost from './components/Admin/Dashboard/EditPost/EditPost'
import ErrorPage from './components/ErrorPage/ErrorPage'
import EditProfile from './components/EditProfile/EditProfile'
import Wrapper from './hoc/Wrapper/Wrapper'
import AdminLayout from './hoc/AdminLayout/AdminLayout'

class App extends Component {

  componentDidMount (){
    this.props.onTryAutoSignupAdmin()
    this.props.onTryAutoSignupUser()
  }

  render(){
    let routes = null
    if(this.props.isUserAuthenticated){
      routes = (
      <Switch>
        <Route path="/Error" exact component={ErrorPage}/>
        <Layout>
          <Route path="/" exact component={Home}/>
          <Route path="/Blog" exact component={Blog}/>
          <Route path="/Tweet" exact component={Tweet}/>
          <Route path="/Blog/Post/:id" component={Post}/>
          <Route path="/Profile" exact component={EditProfile}/>
        </Layout>
          <Redirect to="/"/>
      </Switch>
      )
    }
    else if (this.props.isAdminAuthenticated){
      routes = (
      <Switch>
        <Route path="/Error" exact component={ErrorPage}/>
        <AdminLayout>
          <Route path="/" exact component={Posts}/>
          <Route path="/NewPost" exact component={newPost}/>
          <Route path="/EditPost/:id" component={editPost}/>
        </AdminLayout>
        <Redirect to="/"/>
      </Switch>
      )
    }else{
      routes = (
        <Switch>
          <Route path="/Login" exact component={Login}/>
          <Route path="/Admin/Login" exact component={LoginAdmin}/>
          <Route path="/Signup" exact component={SignUp}/>
          <Route path="/Error" exact component={ErrorPage}/>
          <Layout>
          <Route path="/" exact component={Home}/>
          <Route path="/Blog" exact component={Blog}/>
          <Route path="/Tweet" exact component={Tweet}/>
          <Route path="/Blog/Post/:id" component={Post}/>
          <Redirect to="/"/>
          </Layout>
        </Switch>
      )
    }
    return (
      <Wrapper>
        {routes}
      </Wrapper>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAdminAuthenticated: state.adminAuth.xtoken !== null,
    isUserAuthenticated: state.userAuth.token !== null,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignupAdmin: () => dispatch(actions.adminAuthCheckState()),
    onTryAutoSignupUser: () => dispatch(actions.userAuthCheckState())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))