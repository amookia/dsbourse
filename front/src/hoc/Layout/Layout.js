import React , { Component } from 'react'
import * as actions from '../../store/actions/index'
import Wrapper from '../Wrapper/Wrapper'
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'
import Footer from '../../components/Footer/Footer'
import {connect} from 'react-redux'
import Fab from '../../components/UI/Fab/Fab'

class Layout extends Component{
    state = {
        showSideDrawer: false,
        dropDown: false,
    }

    sideDrawerClosedHandler = () => {
        this.setState( { showSideDrawer: false } )
    }

    sideDrawerToggleHandler = () => {
        this.setState( ( prevState ) => {
            return { showSideDrawer: !prevState.showSideDrawer };
        });
    }

    render () {
        const onDropDown = () =>{
            this.setState({dropDown: !this.state.dropDown})
        }
        return (
            <Wrapper className={classes.layout}>
                <Toolbar 
                    drawerToggleClicked={this.sideDrawerToggleHandler}
                    isAdminAuth={this.props.isAdminAuthenticated}
                    isUserAuth={this.props.isUserAuthenticated}
                    signOut={this.props.isAdminAuthenticated ? this.props.onSignOutAdmin : this.props.onSignOutUser}
                    dropDown={this.state.dropDown}
                    onDropDown={onDropDown}
                /> 
                <SideDrawer
                    isAdminAuth={this.props.isAdminAuthenticated}
                    isUserAuth={this.props.isUserAuthenticated}
                    open={this.state.showSideDrawer}
                    closed={this.sideDrawerClosedHandler}
                    signOut={this.props.isAdminAuthenticated ? this.props.onSignOutAdmin : this.props.onSignOutUser}
                />
                <Fab/>
                <main className={classes.Content} onClick={() => {this.setState({dropDown: false})}}>
                    {this.props.children}
                </main>
                <Footer/>
            </Wrapper>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAdminAuthenticated: state.adminAuth.xtoken !== null,
        isUserAuthenticated: state.userAuth.token !== null,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSignOutAdmin: () => dispatch(actions.adminLogout()),
        onSignOutUser: () => dispatch(actions.userLogout())

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Layout)