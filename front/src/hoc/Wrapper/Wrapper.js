import React , { Component } from 'react'

import classes from './Wrapper.css'

class Wrapper extends Component {
    render(){
        const Wrapper = [classes.Wrapper, this.props.className]
        return(
            <div onClick={this.props.onClick} style={this.props.style} className={Wrapper.join(' ')}>{
                this.props.children
            }</div>
        )
    }
};

export default Wrapper;