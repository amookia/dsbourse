import * as actionTypes from './actionTypes'
import * as actions from './index'
import axios from '../../axios'

export const authStart = () =>{
    return {
        type: actionTypes.USER_AUTH_START
    }
}

export const authSuccess = (token) => {
    localStorage.clear()
    localStorage.setItem('token', token)
    return {
        type: actionTypes.USER_AUTH_SUCCESS,
        token: token
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.USER_AUTH_FAIL,
        error: error
    }
}


export const userLogout = () => {
    localStorage.removeItem('token');
    return {
        type: actionTypes.USER_AUTH_LOGOUT
    };
};


export const userAuth = (userName, password, history) => {
    return dispatch => {
        dispatch(authStart())
        const data = {
            "username" : userName,
            "password" : password
        }
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        axios.post('/api/users/login', data, config)
            .then(response => {
                if (response.data.token){
                    dispatch(actions.adminLogout())
                    dispatch(userLogout())
                    dispatch(authSuccess(response.data.token))
                    dispatch(actions.profileFetch(response.data.token,"user"))
                    console.log('loged in')
                    history.push('/')
                }else{
                    dispatch(authFail(response.data.error))
                }
            })
            .catch(error => {
                console.log(error.response)
                dispatch(authFail(error.response))
                
            })
    }
}

export const userAuthCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if (!token) {
            dispatch(userLogout());
        } else {
            dispatch(authSuccess(token));
            dispatch(actions.profileFetch(token,"user"))
        }   
    };
};