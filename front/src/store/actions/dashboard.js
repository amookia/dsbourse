import * as actions from './actionTypes'
import axios from '../../axios'

export const dashboardFetchPostsStart = () =>{
    return {
        type: actions.DASHBOARD_FETCH_POSTS_START
    }
}

export const dashboardFetchPostsSuccess = (postsData) =>{
    return {
        type: actions.DASHBOARD_FETCH_POSTS_SUCCESS,
        posts: postsData
    }
}

export const dashboardFetchPostsFail = (error) =>{
    return {
        type: actions.DASHBOARD_FETCH_POSTS_FAIL,
        error: error
    }
}

export const dashboardUpdatePagination = (activated) =>{
    return {
        type: actions.DASHBOARD_UPDATE_PAGINATION,
        activated: activated
    }
}

export const dashboardFetchPosts = () =>{
    return dispatch => {
        dispatch(dashboardFetchPostsStart())
        axios.get('/api/blog/index')
        .then(response => {
            console.log(response.data)
            if (!response.data.msg){
                dispatch(dashboardFetchPostsSuccess(response.data.posts))
            }else{
                dispatch(dashboardFetchPostsFail(response.data.msg))
            }
        })
        .catch(error => {
            dispatch(dashboardFetchPostsFail(error))
        })
    }
}