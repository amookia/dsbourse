import * as actionTypes from './actionTypes'
import * as actions from './index'
import axios from '../../axios'

export const authStart = () =>{
    return {
        type: actionTypes.ADMIN_AUTH_START
    }
}

export const authSuccess = (xtoken) => {
    localStorage.clear()
    localStorage.setItem('xtoken', xtoken)
    return {
        type: actionTypes.ADMIN_AUTH_SUCCESS,
        xtoken: xtoken
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.ADMIN_AUTH_FAIL,
        error: error
    }
}


export const adminLogout = () => {
    localStorage.removeItem('xtoken');
    return {
        type: actionTypes.ADMIN_AUTH_LOGOUT
    };
};


export const adminAuth = (userName, password, calbk) => {
    return dispatch => {
        dispatch(authStart())
        const data = {
            "username" : userName,
            "password" : password
        }
        axios.post('/api/adminisgod/login', data)
            .then( response => {
                if (response.data.token){
                    dispatch(actions.userLogout())
                    dispatch(adminLogout())
                    dispatch(authSuccess(response.data.token))
                    dispatch(actions.profileFetch(response.data.token,"admin"))
                    dispatch(calbk())
                }else{
                    dispatch(authFail(response.data.error))
                }
            })
            .catch(error => {
                dispatch(authFail(error))
            })
    }
}

export const adminAuthCheckState = () => {
    return dispatch => {
        const xtoken = localStorage.getItem('xtoken');
        if (!xtoken) {
            dispatch(adminLogout());
        } else {
            dispatch(authSuccess(xtoken));
            dispatch(actions.profileFetch(xtoken,"admin"))

        }   
    };
};