import * as actionTypes from './actionTypes'
import axios from '../../axios'

const profileFetchStart = () =>{
    return{
        type: actionTypes.PROFILE_FETCH_START
    }
}

const profileFetchSuccess = (profile) =>{
    return{
        type: actionTypes.PROFILE_FETCH_SUCCESS,
        profile: profile
    }
}

const profileFetchError = (error) => {
    return{
        type: actionTypes.PROFILE_FETCH_FAIL,
        error: error
    }
} 

export const profileFetch = (token,privilage) =>{
    return dispatch =>{
        dispatch(profileFetchStart())
        const config = {
            headers: {       
                "Authorization" : token,
            }
        }
        if(privilage == "admin"){
            axios.get('/api/adminisgod/me',config)
            .then(response =>{
                dispatch(profileFetchSuccess(response.data))
            })
            .catch(error =>{
                dispatch(profileFetchError())
            })
        }else if (privilage == "user"){
            axios.get('/api/users/me',config)
            .then(response =>{
                dispatch(profileFetchSuccess(response.data))

            })
            .catch(error =>{
                dispatch(profileFetchError())
            })
        }   
    }
}