import * as actions from './actionTypes'
import axios from '../../axios'

export const blogsFetchPostsStart = () =>{
    return {
        type: actions.BLOGS_FETCH_POSTS_START,
    }
}

export const blogsFetchPostsSuccess = (postsData) =>{
    return {
        type: actions.BLOGS_FETCH_POSTS_SUCCESS,
        posts: postsData
    }
}

export const blogsFetchPostsFail = (error) =>{
    return {
        type: actions.BLOGS_FETCH_POSTS_FAIL,
        error: error
    }
}

export const blogsFetchIndexStart = () =>{
    return {
        type: actions.BLOGS_FETCH_INDEX_START,
    }
}

export const blogsFetchIndexSuccess = (postsData) =>{
    return {
        type: actions.BLOGS_FETCH_INDEX_SUCCESS,
        posts: postsData
    }
}

export const blogsFetchIndexFail = (error) =>{
    return {
        type: actions.BLOGS_FETCH_INDEX_FAIL,
        error: error
    }
}

export const blogsUpdatePagination = (activated) =>{
    return {
        type: actions.BLOGS_UPDATE_PAGINATION,
        activated: activated
    }
}


export const blogsFetchPosts = (page) =>{
    return dispatch => {
        dispatch(blogsFetchPostsStart())
        axios.get(`/api/blog/page/${page}`)
        .then(response => {
            console.log(response.data)
            if (!response.data.msg){
                dispatch(blogsFetchPostsSuccess(response.data))
            }else{
                dispatch(blogsFetchPostsFail(response.data.msg))
            }
        })
        .catch(error => {
            dispatch(blogsFetchPostsFail(error))
        })
    }
}

export const blogsFetchIndex = () =>{
    return dispatch => {
        dispatch(blogsFetchIndexStart())
        axios.get('/api/blog/index')
        .then(response => {
            console.log(response.data)
            if (!response.data.msg){
                dispatch(blogsFetchIndexSuccess(response.data))
            }else{
                dispatch(blogsFetchIndexFail(response.data.msg))
            }
        })
        .catch(error => {
            dispatch(blogsFetchIndexFail(error))
        })
    }
}