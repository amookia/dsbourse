import * as actionTypes from './../actions/actionTypes'
import {updateObject} from './../utility'

const initialState = {
    avatar : null,
    name : null,
    phone : null,
    userName : null,
    loading: false,
    error: null,
}

const profileFetchStart = (state,action) => {
    return updateObject(state,{loading: true})
}

const profileFetchSuccess = (state,action) => {
    return updateObject(state,{
        avatar: action.profile.avatar,
        name: action.profile.name,
        phone: action.profile.phone_number,
        userName: action.profile.username,
        loading: false
    })
}

const profileFetchFail = (state,action) => {
    return updateObject(state,{error: action.error,loading: false})
}

const reducer = (state = initialState,action) =>{
    switch(action.type){
        case actionTypes.PROFILE_FETCH_START: return profileFetchStart(state,action)
        case actionTypes.PROFILE_FETCH_SUCCESS: return profileFetchSuccess(state,action)
        case actionTypes.PROFILE_FETCH_FAIL: return profileFetchFail(state,action)
        default: return state 
    }
}

export default reducer
