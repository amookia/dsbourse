import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
    xtoken: null,
    loading: false,
    error: null,
}

const authStart = (state, action) => {
    return updateObject( state, { loading: true})
}

const authSuccess = (state, action) => {
    return updateObject(state,{
            xtoken: action.xtoken,
            loading: false
    })
}

const adminLogout = (state, action) => {
    return updateObject(state, { xtoken: null})
}

const authFail = (state, action) => {
    return updateObject(state, { 
        loading: false,
        error: action.error,
    } )
}

const reducer = (state = initialState, action) => {
    switch( action.type ){
        case actionTypes.ADMIN_AUTH_START: return authStart(state, action)
        case actionTypes.ADMIN_AUTH_SUCCESS: return authSuccess(state, action)
        case actionTypes.ADMIN_AUTH_FAIL: return authFail(state, action)
        case actionTypes.ADMIN_AUTH_LOGOUT: return adminLogout(state, action)
        default: return state
    }
}

export default reducer