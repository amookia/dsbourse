import * as actionTypes from '../actions/actionTypes'
import {updateObject} from '../utility'

const initialState = {
    posts: null,
    loading: false,
    error: null,
    totalPages: null,
    activated: 1,
}

const dashboardFetchPostsStart = (state,action) => {
    return updateObject(state,{loading: true})
}

const dashboardFetchPostsSuccess = (state,action) => {
    return updateObject(state,{
        posts: action.posts,
        totalPages: Math.ceil(action.posts.length / 5),
        loading: false
    })
}

const dashboardUpdatePagination = (state,action) => {
    return updateObject(state,{
        activated: action.activated
    })
}

const dashboardFetchPostsFail = (state,action) => {
    return updateObject(state,{
        loading: false,
        error: action.error
    })
}

const reducer = (state= initialState,action) => {
    switch(action.type){
        case actionTypes.DASHBOARD_FETCH_POSTS_START : return dashboardFetchPostsStart(state,action)
        case actionTypes.DASHBOARD_FETCH_POSTS_SUCCESS: return dashboardFetchPostsSuccess(state,action)
        case actionTypes.DASHBOARD_FETCH_POSTS_FAIL: return dashboardFetchPostsFail(state,action)
        case actionTypes.DASHBOARD_UPDATE_PAGINATION: return dashboardUpdatePagination(state,action)
        default: return state
    }
}

export default reducer