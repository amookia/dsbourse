import * as actionTypes from '../actions/actionTypes'
import { updateObject } from '../utility'

const initialState = {
    token: null,
    loading: false,
    error: null,
}

const authStart = (state, action) => {
    return updateObject( state, { loading: true})
}

const authSuccess = (state, action) => {
    return updateObject(state,{
            token: action.token,
            loading: false
    })
}

const adminLogout = (state, action) => {
    return updateObject(state, { token: null})
}

const authFail = (state, action) => {
    return updateObject(state, { 
        loading: false,
        error: action.error,
    } )
}

const reducer = (state = initialState, action) => {
    switch( action.type ){
        case actionTypes.USER_AUTH_START: return authStart(state, action)
        case actionTypes.USER_AUTH_SUCCESS: return authSuccess(state, action)
        case actionTypes.USER_AUTH_FAIL: return authFail(state, action)
        case actionTypes.USER_AUTH_LOGOUT: return adminLogout(state, action)
        default: return state
    }
}

export default reducer