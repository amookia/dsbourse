import * as actionTypes from '../actions/actionTypes'
import {updateObject} from '../utility'

const initialState = {
    posts: null,
    loading: false,
    error: null,
    totalPages: null,
    activated: 1,
}

const blogsFetchPostsStart = (state,action) => {
    return updateObject(state,{loading: true})
}

const blogsFetchIndexStart = (state,action) => {
    return updateObject(state,{loading: true})
}

const blogsFetchPostsSuccess = (state,action) => {
    return updateObject(state,{
        posts: action.posts.posts,
        loading: false
    })
}

const blogsFetchIndexSuccess = (state,action) => {
    return updateObject(state,{
        posts: action.posts.posts,
        totalPages: Math.ceil(action.posts.total / 8),
        loading: false
    })
}

const blogsUpdatePagination = (state,action) => {
    return updateObject(state,{
        activated: action.activated
    })
}

const blogsFetchPostsFail = (state,action) => {
    return updateObject(state,{
        loading: false,
        error: action.error
    })
}

const blogsFetchIndexFail = (state,action) => {
    return updateObject(state,{
        loading: false,
        error: action.error
    })
}

const reducer = (state= initialState,action) => {
    switch(action.type){
        case actionTypes.BLOGS_FETCH_POSTS_START : return blogsFetchPostsStart(state,action)
        case actionTypes.BLOGS_FETCH_POSTS_SUCCESS: return blogsFetchPostsSuccess(state,action)
        case actionTypes.BLOGS_FETCH_POSTS_FAIL: return blogsFetchPostsFail(state,action)
        case actionTypes.BLOGS_FETCH_INDEX_START : return blogsFetchIndexStart(state,action)
        case actionTypes.BLOGS_FETCH_INDEX_SUCCESS: return blogsFetchIndexSuccess(state,action)
        case actionTypes.BLOGS_FETCH_INDEX_FAIL: return blogsFetchIndexFail(state,action)
        case actionTypes.BLOGS_UPDATE_PAGINATION: return blogsUpdatePagination(state,action)
        default: return state
    }
}

export default reducer