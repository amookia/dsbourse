import React,{Component} from 'react'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import classes from './ErrorPage.css'
import img404 from '../../assets/images/main/404.png'
import background404 from '../../assets/images/main/404-background.png'

class ErrorPage extends Component{
    render(){
        return(
            <Wrapper className={classes.errorBox}>
                <img src={background404} className={classes.errorTopShape}/>
                <div className={classes.imageContainer}>
                    <img className={classes.image} src={img404}/>
                </div>
            </Wrapper>
        )}
} 

export default ErrorPage