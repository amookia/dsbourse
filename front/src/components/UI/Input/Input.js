import React from 'react'
import classes from './Input.css'

const Input = (props) =>{
    let inputElement = null
    let inputClasses = [classes.input, props.className]

    if (props.isValid && props.shouldValidate && props.touched) {
        inputClasses.push(classes.invalid);
    }
    switch(props.elementType){
        case ( 'input' ):
            inputElement = (
                <input ref={props.ref} readOnly={props.readOnly} type={props.elementConfig.type} className={inputClasses.join(' ')} 
                placeholder={props.elementConfig.placeholder} 
                value={props.value} 
                onChange={props.onChange}/>
            )
            break
        case ( 'textarea' ):
            inputElement = <textarea
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                type={props.elementConfig.type}
                onChange={props.onChange} />;
            break;
        default:
            inputElement = (
                <input className={inputClasses.join(' ')} 
                placeholder={props.elementConfig.placeholder} 
                value={props.value} 
                type={props.elementConfig.type}
                onChange={props.onChange}/>
            )
            break

    }
    return inputElement

}

export default Input