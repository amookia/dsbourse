import React from 'react'
import classes from './Button.css'

const Button = (props) => {
    let buttonElement = null
    let buttonClasses = null
    switch(props.buttonType){
        case "signIn" :
            buttonClasses = [classes.button, classes.signIn, props.className]
            buttonElement = (
                <button
                className={buttonClasses.join(" ")}
                onClick={props.onClick}>
                    {props.children}
                    </button>
            )
            break
        case "signUp" :
            buttonClasses = [classes.button, classes.signUp, props.className]
            buttonElement = (
                <button
                className={buttonClasses.join(" ")}
                onClick={props.onClick}>
                    {props.children}
                </button>
            )
        case "signout" :
            buttonClasses = [classes.button, classes.signUp, props.className]
            buttonElement = (
            <button
                className={buttonClasses.join(" ")}
                onClick={props.onClick}>
                {props.children}
            </button>
            )
    }
    return buttonElement
}

export default Button