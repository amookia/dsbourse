import React from 'react'
import classes from './Pagination.css'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import back from '../../../assets/images/icons/forward.png'
import forward from '../../../assets/images/icons/back.png'

const Pagination = (props) =>{
    let activated = props.activated
    let pagination = []

    if(props.totalPages > 5){
        let b = 0
        for(let i=activated;i<=props.totalPages;i++){
            if (b>5){
                break
            }
            if(i==activated){
                pagination.push(
                    <button key={i} onClick={() => props.page(i)} className={classes.active}>{i}</button>
                )
            }else{
                pagination.push(
                    <button key={i} onClick={() => props.page(i)} >{i}</button>
                )
            }
            b++
        }
    }else{
        for(let i=1;i<=props.totalPages;i++){
            if(i==activated){
                pagination.push(
                    <button key={i} onClick={() => props.page(i)} className={classes.active}>{i}</button>
                )
            }else{
                pagination.push(
                    <button key={i} onClick={() => props.page(i)} >{i}</button>
                )
            }
        }
    }    
    
    return(
        <Wrapper className={classes.center}>
            <div className={classes.pagination}>
                <img src={forward} onClick={props.back}/>
                {pagination}
                <img src={back} onClick={props.forward}/>
            </div>
        </Wrapper>
    )
}

export default Pagination