import React, { Component } from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import searchImg from '../../../assets/images/icons/search.png'
import classes from './SearchBox.css'

class SearchBox extends Component{

    render(){
        return(
            <Wrapper className={classes.searchBox}>
                <input type="text" placeholder="سهام" />
                <button className={classes.searchButton}><img src={searchImg}/></button>
            </Wrapper>
        )
    }
}

export default SearchBox