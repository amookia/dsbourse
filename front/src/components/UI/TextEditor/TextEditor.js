import React, {Component} from 'react';
import JoditEditor from "jodit-react";

class TextEditor extends Component {
  render(){
    return (
      <JoditEditor
        config={this.props.config}
        value={this.props.value}
        onChange={newContent => {this.props.handleContent(newContent)}}
      />
  );
  }
}


export default TextEditor
