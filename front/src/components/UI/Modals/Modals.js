import React,{Component} from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import Backdrop from '../Backdrop/Backdrop'
import classes from './Modals.css'



class Modals extends Component{
    render(){
        const modalClasses = [classes.modal, this.props.classes]
        const modalContainer = [classes.container, this.props.container]
        return(
            <Wrapper className={modalContainer.join(" ")}>
                <div className={modalClasses.join(" ")}>
                    {this.props.content}
                </div>
                <Backdrop clicked={this.props.onClick} show={true}/>
            </Wrapper>
        )
    }
}

export default Modals