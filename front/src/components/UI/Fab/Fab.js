import React,{useRef} from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import classes from './Fab.css'
import fab from '../../../assets/images/icons/fab.png'

const Fab = props => {
    const topFunction = () => {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }

    const fabRef = useRef();

    window.onscroll = () =>{
        if (fabRef.current != null){
            if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0 ) {
                fabRef.current.style.display = "flex";
            } else {
                fabRef.current.style.display = "none";
            }
        }
    }
    
    return(
        <Wrapper>
            <span ref={fabRef} onClick={topFunction} className={classes.fab}>
                <img src={fab}/>
            </span>
        </Wrapper>
    )
}
export default Fab