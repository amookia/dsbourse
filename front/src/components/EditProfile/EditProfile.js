import React,{Component} from 'react'
import classes from './EditProfile.css'
import {axios2} from '../../axios'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'
import {connect} from 'react-redux'
import Modals from '../UI/Modals/Modals'
import Spinner from '../UI/Spinner/Spinner'
import * as actions from '../../store/actions/index'

class EditProfile extends Component{
    state = {
        binary: null,
        profile : this.props.avatar,
        controls : {
            name: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "نام",
                },
                value: this.props.name,
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            userName: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "نام کاربری",
                },
                value: this.props.userName,
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            }
        },
        onEdit: false,
        showModal: false,
        loading: false,
        error: null,
    }
    checkValidity(value, rules) {
        let isValid = true

        if(!rules) {
            return true
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }
        return isValid

    }
    inputChangedHandler( event, controlName ){
        const updatedControls = {
            ...this.state.controls,
            [controlName] : {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true,
            }
        }
        this.setState({controls: updatedControls})
    }   
    onEditButton = () => {
        this.setState({onEdit : true})
    }
    onEditUserProfile = () =>{
        this.setState({showModal: true})
        this.setState({loading: true})
        if(this.state.profile != this.props.avatar){
            const formData = new FormData();
            formData.append('file',this.state.binary[0]);
            const config = {
                headers: {
                    'Authorization': this.props.token,
                    'content-type': 'multipart/form-data'
                }
            };
            axios2.post("/api/users/editprofile/avatar",formData,config)
            .then(response => {
                this.props.onTryAutoSignupUser()
                this.setState({loading: false})
            })
            .catch(error =>{
                this.setState({error: error.response.data.error})
            })
        }
        let data = {}
        if (this.props.userName != this.state.controls.userName.value) {
            data.username = this.state.controls.userName.value
        }
        if (this.props.name != this.state.controls.name.value) {
            data.name = this.state.controls.name.value
        }
        if (this.props.name != this.state.controls.name.value || this.props.userName != this.state.controls.userName.value) {
            const config = {
                headers: {
                    'Authorization': this.props.token,
                }
            };
            axios2.patch("/api/users/editprofile",data,config)
            .then(response => {
                this.props.onTryAutoSignupUser()
                this.setState({loading: false})
            })
            .catch(error =>{
                this.setState({error: error.response.data.error})
            })
        }
    }
    render(){
        const readURL = async (e) => {
            e.preventDefault()
            this.setState({binary: e.target.files})
            const reader = new FileReader()
            reader.onload = async (e) => { 
              this.setState({profile: e.target.result})
            };
            reader.readAsDataURL(e.target.files[0])
        }
        const formElementArray = []
        for (let key in this.state.controls){
            formElementArray.push({
                id: key,
                config: this.state.controls[key],
            })
        }
        console.log(this.state.controls)
        const form = formElementArray.map(formElement =>(
            <Input
            readOnly={this.state.onEdit ? false : true}
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            isValid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            onChange={(event) => this.inputChangedHandler(event, formElement.id)}
            />)
        )
        let button = <Button buttonType={"signIn"} onClick={this.onEditButton}>تغیر</Button>
        
        if(this.state.onEdit){
            button = <Button buttonType={"signUp"} onClick={this.onEditUserProfile}>ثبت</Button>
        }
        let modal = <Modals
        classes={`${classes.modalSuccess} ${classes.modal}`}
        content={
            (
                <div>
                    <i class="fas fa-check-circle"></i>
                    <h1>باتشکر</h1>
                    <p>اطلاعات جدید با موفقیت ثبت شد</p>
                    <Button buttonType={"signIn"} onClick={() => this.props.history.push("/")}>تایید</Button>
                </div>
            )
        }
        />
        if(this.state.loading == true){
            modal = <Modals  classes={classes.modal} content={<Spinner/>}/>
        }
        if(this.state.error != null){
            modal = <Modals
            classes={`${classes.modalError} ${classes.modal}`}
            content={
                (
                    <div>
                        <i class="fas fa-exclamation-circle"></i>
                        <h1>خطا</h1>
                        <p>{this.state.error}</p>
                        <Button buttonType={"signIn"} onClick={() => {this.setState({error: null,showModal: false})}}>تایید</Button>
                    </div>
                )
            }
            />
        }
          
        return(
            <Wrapper className={classes.mainContainer}>
                {this.state.showModal ? modal : null}
                <div className={classes.profile}>
                    <div className={classes.container}>
                            <h1>تصویر پروفایل</h1>
                            <div className={classes.avatarUpload}>
                                <div className={classes.avatarEdit}>
                                    <input type='file' id="imageUpload" onChange={e => readURL(e)} accept=".png, .jpg, .jpeg" />
                                    {this.state.onEdit ? <label for="imageUpload"></label> : null}
                                </div>
                                <div className={classes.avatarPreview}>
                                    <div id="imagePreview" style={{backgroundImage: `url(${this.state.profile})`}}>
                                    </div>
                                </div>
                            </div>
                            <small>پیش نمایش</small>
                        </div>
                        <div className={classes.formContainer}>
                            {form}
                            {button}
                        </div>
                </div>
            </Wrapper>
        )
    }
}

const mapStateToProps = state => {
    return {
        avatar: state.profile.avatar,
        name: state.profile.name,
        userName: state.profile.userName,
        token: state.userAuth.token
    }
}

const mapDispatchToProps = dispatch => {
    return {
      onTryAutoSignupUser: () => dispatch(actions.userAuthCheckState())
    }
  }

export default connect(mapStateToProps,mapDispatchToProps)(EditProfile)