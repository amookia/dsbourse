import React from 'react'
import classes from './NavigationItem.css'
import { NavLink } from 'react-router-dom'

const NavigationItem = (props) => {
    let navLinks = (
        <NavLink 
        to={props.link}
        exact={props.exact}
        activeClassName={classes.active}>{props.children}</NavLink>
    )
    if (props.link == '/Logout'){
        navLinks = (
            <span onClick={props.signOut}>{props.children}</span>
        )
    }
    return(
    <li className={classes.NavigationItems}>
        <div className={classes.NavigationItem}>
            {navLinks}
        </div>
    </li>
    )
}

export default NavigationItem