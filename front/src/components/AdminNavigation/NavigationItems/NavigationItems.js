import React from 'react'
import classes from './NavigationItems.css'
import NavigationItem from './NavigationItem/NavigationItem'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import dashboardImg from '../../../assets/images/icons/dashboard.png'
import addPostsImg from '../../../assets/images/icons/addPosts.png'
import logoutImg from '../../../assets/images/icons/logout.png'

const NavigationItems = (props) => {
    let navigationitems
    if (props.isAdminAuth){
        navigationitems = (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/" exact>
                <img src={dashboardImg}/>
                داشبورد
            </NavigationItem>
            <NavigationItem  className={classes.imgContainer} link="/NewPost">
                <img src={addPostsImg}/>
                پست جدید
            </NavigationItem>
            <NavigationItem  className={classes.imgContainer} link="/Logout"  signOut={props.signOut}>
                <img src={logoutImg}/>
                خروج
            </NavigationItem>
            {/* <NavigationItem link="/Posts">پست ها</NavigationItem> */}
        </ul>
        )
    }
    return(
    <ul className={classes.NavigationItems}>
        {navigationitems}
    </ul>
    )
}

export default NavigationItems