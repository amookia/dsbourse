import React , { Component } from 'react'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import classes from './Toolbar.css'
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle'
import Profile from "./Profile/Profile"
import topShape from "./../../../assets/images/main/topShape.png"

class Toolbar extends Component {
     render(){
        const navclasses = [classes.DesktopOnly,classes.itemsLogo]
        return (
            <header className={classes.Toolbar}>
                <div className={classes.ToolbarItems}>
                    <DrawerToggle clicked={this.props.drawerToggleClicked}/>
                    <nav className={navclasses.join(' ')}>
                        <Logo type={'admin'}/>
                        <NavigationItems 
                        signOut={this.props.signOut}
                        loggedin={true}
                        isAdminAuth={this.props.isAdminAuth}
                        isUserAuth={this.props.isUserAuth}/>
                    </nav>
                </div>
            </header>
        )
    }
}

export default Toolbar