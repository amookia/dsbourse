import React , { Component } from 'react'
import Wrapper from '../../../../hoc/Wrapper/Wrapper'
import { connect } from 'react-redux'
import classes from './Profile.css'
import withErrorHandler from '../../../../hoc/withErrorHandler/withErrorHandler'
import ADMIN_PROFILE_IMAGE from '../../../../assets/images/Profiles/admin.webp'
import axios from '../../../../axios'
import { Link } from 'react-router-dom'
import LazyImage from '../../../UI/LazyImage/LazyImage'
import imageNotFound from '../../../../assets/images/articles/imageNotFound.webp'

class Profile extends Component{
    state = {
        dropDown: false
    }
    render(){

        let profile_image = [classes.image,classes.item]
            
        let content = (
            <Wrapper >
                <Link className={classes.SignUp} to={'/Login'}>ورود</Link>
                <Link className={classes.login} to={'/SignUp'}>ثبت نام</Link>
            </Wrapper>
            )
        if(this.props.isAdminAuth){
            content = (
            <Wrapper >
                <LazyImage className={profile_image.join(' ')} src={ADMIN_PROFILE_IMAGE} onClick={this.props.onDropDown} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                {this.props.dropDown ?<div className={classes.dropDown}> 
                    <div className={classes.infoContainer}>
                        <p>{`@admin`}</p>
                        <p>{`مدیر`}</p>
                    </div>
                    <div className={classes.controlers}>
                        <p onClick={this.props.onClick}>خروج</p>
                    </div>
                </div> : null}
            </Wrapper>
            )
        }
        else if(this.props.isUserAuth){
            content = (
                <Wrapper >
                    <LazyImage className={profile_image.join(' ')} src={this.props.avatar} onClick={this.props.onDropDown} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                    {this.props.dropDown ?<div className={classes.dropDown}> 
                        <div className={classes.infoContainer}>
                            <p>{`@${this.props.userName}`}</p>
                            <p>{this.props.name}</p>
                        </div>
                        <div className={classes.controlers}>
                            <Link to={'/Profile'} className={classes.settings}>تنظیمات</Link>
                            <p onClick={this.props.onClick}>خروج</p>
                        </div>
                    </div> : null}
                </Wrapper>
            )   
        }
        if(this.props.loading){
            content = (
                <Wrapper className={classes.loading}>
                   <div className={classes.ldsDualRing}></div>
                </Wrapper>
            )
        }
        return(
            <Wrapper className={classes.profileContainer}>
                {content}
            </Wrapper>
        )
    }
}

const mapStateToProps = state => {
    return {
        avatar: state.profile.avatar,
        name: state.profile.name,
        phone: state.profile.phone,
        userName: state.profile.userName,
        loading: state.profile.loading,
        error: state.profile.error
    }
}

export default connect(mapStateToProps,null)(withErrorHandler(Profile,axios))
