import React, { Component } from 'react'
import footerShape from '../../assets/images/main/footer.png'
import classes from './Footer.css'

const Footer = (props) => (
    <div className={classes.footerBox}>
        <div className={classes.footer}>
            <img src={footerShape}/>
        </div>
        <p>
            تمامی حقوق این سایت متعلق به ‌دستیاربورس © می باشد
        </p>
    </div>
)

export default Footer