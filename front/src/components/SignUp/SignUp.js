import React,{ Component } from 'react'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import Input from '../UI/Input/Input'
import Button from '../UI/Button/Button'
import classes from './SignUp.css'
import image from '../../assets/images/main/login.webp'
import {axios2} from '../../axios'
import Modal from '../UI/Modals/Modals'
import * as actions from '../../store/actions/index'
import {connect} from 'react-redux'
import sun from '../../assets/images/main/sun.png'
import buildings from '../../assets/images/main/buildings.png'
import pig from '../../assets/images/main/pig.png'
import padlock from '../../assets/images/icons/padlock.png'
import padlocks from '../../assets/images/icons/padlocks.png'
import user from '../../assets/images/icons/user.png'
import username from '../../assets/images/icons/username.png'
import phone from '../../assets/images/icons/phone.png'


class SignUp extends Component{
    state = {
        button : {
            signIn: {
                buttonType: "signIn",
                value : "ورود",
            },
            signUp: {
                buttonType: "signUp",
                value : "ثبت نام",
            },
        },
        codeCheck: {
            code: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "کد ارسال شده",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
        },
        controls : {
            name: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "نام",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            userName: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "نام کاربری",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            phone: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "شماره تلفن",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            password: {
                elementType: "input",
                elementConfig: {
                    type: "password",
                    placeholder: "رمز عبور",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            passwordRetry : {
                elementType: "input",
                elementConfig: {
                    type: "password",
                    placeholder: "تکرار رمز عبور",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            }
        },
        error: '',
        showModal: false,
        timer: ''
    }

    startTimer(duration) {
        var timer = duration, minutes, seconds;
        setInterval( () => {
            minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);
    
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            
            this.setState({timer: minutes + ":" + seconds})    
            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    checkValidity(value, rules) {
        let isValid = true

        if(!rules) {
            return true
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }
        return isValid

    }


    buttonOnClickHandler(event, buttonName){
        event.preventDefault()
        if (buttonName == "signIn"){
            this.props.history.push('/Login')
        }
        if(buttonName == "signUp"){
            const name = this.state.controls.name
            const userName = this.state.controls.userName
            const password = this.state.controls.password
            const password_sec = this.state.controls.passwordRetry
            const phone = this.state.controls.phone
            if (password.value == password_sec.value && name.valid && userName.valid && password.valid && phone.valid && password_sec.valid){
                console.log("ok")
                const info = {
                    'name' : name.value,
                    'username' : userName.value,
                    'password' : password.value,
                    'phone_number' : phone.value,
                }
                this.onSignUpHandler(info)
            }else{
                this.setState({error: "اطلاعات را به درستی وارد کنید"})
            }
            
        }
    }

    onSignUpHandler(info){
        axios2.post('/api/users/register',info)
        .then(response =>{
            console.log(response.data)
            this.startTimer(60 * 5)
            this.setState({showModal: true})
        })
        .catch(error => {
            console.log(error.response.data.error)
            this.setState({error : error.response.data.error})
            if(error.response.data.error == 'کد برای این شماره ارسال شده است'){
                if(this.state.timer !== ''){
                    this.startTimer(60 * 5)
                }
                this.setState({showModal: true})
            }
        })
        
        

    }

    codeCheckChangedHandler(event, controlName){
        const updatedControls = {
            ...this.state.codeCheck,
            [controlName] : {
                ...this.state.codeCheck[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.codeCheck[controlName].validation),
                touched: true,
            }
        }
        this.setState({codeCheck: updatedControls})
    }

    inputChangedHandler( event, controlName ){
        const updatedControls = {
            ...this.state.controls,
            [controlName] : {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true,
            }
        }
        this.setState({controls: updatedControls})
    }   
    login = (event) =>{
        event.preventDefault()
        this.props.history.push('/login')
    } 
    onCheckPhoneHandler(value,phone){
        const data = {
            "phone_number" : phone,
            "code" : value
        }
        axios2.post("/api/users/register/verify", data)
        .then(respose =>{
            console.log(respose.data)
            this.props.onAuthSuccess(respose.data.token)
            this.props.onUserAuthCheckState()
            this.props.history.push('/')
        })
        .catch(error =>{
            console.log(error.response.data)
        })
    }
    render(){
        const codeElementArray = []
        for (let key in this.state.codeCheck){
            codeElementArray.push({
                id: key,
                config: this.state.codeCheck[key],
            })
        }
        console.log(this.state.codeCheck)
        const code = codeElementArray.map(formElement =>(
            <div className={classes.inputContainer}>
                <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                isValid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                className={classes.signUpInputs}
                onChange={(event) => this.codeCheckChangedHandler(event, formElement.id)}
                />
            </div>
        ))
        const timer = <div>
            <p>{this.state.timer}</p>
        </div>
        const closeClasses = ['fas', 'fa-times', classes.close]
        const modal = <Modal
        classes={classes.modal}
        container= {classes.modalContainer}
        content={
            (   
                <div className={classes.modalContainer}>
                    <lottie-player src={"https://assets1.lottiefiles.com/packages/lf20_d7raikbb.json"}  background={"transparent"}  speed={"1"}  style={{width: "200px" ,height: "200px"}}  loop autoplay></lottie-player>
                    {code}
                    {timer}
                    <Button buttonType={"signUp"} onClick={() => this.onCheckPhoneHandler(this.state.codeCheck.code.value,this.state.controls.phone.value)}>برسی کد</Button>
                </div>
                
            )
        }
        />
        let imageC = [classes.signUpItem, classes.signUpImage]
        const buttonElementArray = []
        for(let key in this.state.button){
            buttonElementArray.push({
                id: key,
                config: this.state.button[key],
            })
        }
        const buttons = buttonElementArray.map(buttonElement =>(
            <Button
            key={buttonElement.id}
            buttonType={buttonElement.config.buttonType}
            onClick={(event) => this.buttonOnClickHandler(event, buttonElement.id)}>{buttonElement.config.value}</Button>
        )) 
        const formElementArray = []
        for (let key in this.state.controls){
            formElementArray.push({
                id: key,
                config: this.state.controls[key],
            })
        }
        const inputIcons = [user,username,phone,padlock,padlocks]
        console.log(this.state.controls)
        const form = formElementArray.map((formElement,index) =>(
            <div className={classes.inputContainer}>
                <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                isValid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                className={classes.signUpInputs}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)}
                />
                <img className={classes.inputIcons} src={inputIcons[index]}/>
            </div>
        ))
        let show = null;
        if(!this.props.loading){
            show = (
                <div className={classes.signUpContainer}>
                    <form className={classes.signUpForm}>
                        <img className={classes.pig} src={pig}/>
                        {form}
                        <div className={classes.formButtons}>
                            {buttons}
                        </div>
                    </form>
                </div>
            )
        }
        if (this.props.error){
            show = (
                <div className={classes.signUpContainer}>
                    <form className={classes.signUpForm}>
                        <img className={classes.pig} src={pig}/>
                        {form}
                        <p className={classes.error}>{this.state.error}</p>
                        <div className={classes.formButtons}>
                            {buttons}
                        </div>
                    </form>
                </div>
            )
        }
        return(
            <Wrapper className={classes.signUpBox}>
                <img className={classes.sun} src={sun}/>
                <img className={classes.buildings} src={buildings}/>
                {this.state.showModal ? modal : null}
                {show}
            </Wrapper>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
      onAuthSuccess: (token) => dispatch(actions.authSuccess(token)),
      onUserAuthCheckState: () =>dispatch(actions.userAuthCheckState())
    }
  }
export default connect(null,mapDispatchToProps)(SignUp)