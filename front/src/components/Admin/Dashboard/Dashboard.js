import React, { Component } from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import * as actions from '../../../store/actions/index'
import { connect } from 'react-redux'
import classes from './Dashboard.css'
import axios from '../../../axios'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import Spinner from '../../UI/Spinner/Spinner'
import Pagination from '../../UI/Pagination/Pagination'

class Dashboard extends Component{
    componentDidMount(){
        this.props.onFetchPosts()
    }
    onNewPostHandler = () =>{
        this.props.history.push("/NewPost")
    }
    onDetailHandler = (id) =>{
        this.props.history.push(`/EditPost/${id}`)
    }
    onDeleteHandler = (id) => {

        const config = {
            headers: {
                'Authorization': this.props.xtoken,
            }
        };
        axios.delete(`/api/adminisgod/post/${id}`,config)
        .then(response =>{
            console.log(response.data)
            this.props.onFetchPosts(this.props.xtoken)
        })
        .catch(error =>{
            console.log(error.response)
        })

    }
    render(){
        let controllerButtons = null
        const postPer = 5
        let posts = <Spinner/>
        let pagination = null
        
        const pageinationBack = () =>{
            if (this.props.activated > 1){
                this.props.onUpdatePagination(this.props.activated - 1)
            }
        }

        const pageinationForward = () =>{
            if(this.props.activated < this.props.totalPages)
            this.props.onUpdatePagination(this.props.activated + 1)
        }

        const pageinationNumber = (page) =>{
            this.props.onUpdatePagination(page)
        }
     
        if (!this.props.loading){
            if (this.props.posts){
                let post = []
                if(this.props.activated === 1){
                    for(let i=0;i < this.props.posts.length;i++){
                        post.push(
                            <tr key={this.props.posts[i].id}>
                            <td>{i+1}</td>
                            <td>{this.props.posts[i].title}</td>
                            <td className={classes.date}>{this.props.posts[i].date}</td>
                            <td>
                                <button className={classes.detailButton} onClick={() => this.onDetailHandler(this.props.posts[i].id)}>بازنویسی</button>
                                <button className={classes.detailButton} onClick={() => this.onDeleteHandler(this.props.posts[i].id)}>حذف</button>    
                            </td>
                        </tr>)
                    }
                }else{  
                    let j = 0               
                    for(let i=(this.props.activated - 1) * postPer;i < this.props.posts.length;i++){
                        if (j > postPer - 1){
                            break
                        }
                        post.push(
                            <tr key={this.props.posts[i].id}>
                            <td>{i+1}</td>
                            <td>{this.props.posts[i].title}</td>
                            <td className={classes.date}>{this.props.posts[i].date}</td>
                            <td>
                                <button className={classes.detailButton} onClick={() => this.onDetailHandler(this.props.posts[i].id)}>بازنویسی</button>
                                <button className={classes.detailButton} onClick={() => this.onDeleteHandler(this.props.posts[i].id)}>حذف</button>    
                            </td>
                        </tr>)
                        j++
                    }
                }
                
                posts = (
                    <div className={classes.postInfo}>
                        <div className={classes.infoTitle}>
                            <h4>اخرین پست ها</h4>
                        </div>
                        <table>
                            <tr>
                                <th>#</th>
                                <th>موضوع</th>
                                <th>تاریخ</th>
                                <th></th>
                            </tr>
                            {post}
                        </table>
                    </div>
                )
                
                pagination = <Pagination iterate={this.props.posts} activated={this.props.activated} totalPages={this.props.totalPages} back={pageinationBack} forward={pageinationForward} page={pageinationNumber}/>
                controllerButtons = (
                    <div className={classes.contollers}>
                        <button className={classes.controllerButtons} onClick={this.onNewPostHandler}>پست جدید</button> 
                    </div>
                )
            }
        }
       
        return(
            <Wrapper className={classes.pannelContainer}>
                <div className={classes.pannelBox}>
                    {controllerButtons}
                    <div className={classes.postBox}>
                      {posts}
                    </div>
                    {pagination}
                </div>
            </Wrapper>
        )
    }
}

const mapStateToProps = state =>{
    return {
        xtoken: state.adminAuth.xtoken,
        posts: state.dashboard.posts,
        loading: state.dashboard.loading,
        error: state.dashboard.error,
        totalPages: state.dashboard.totalPages,
        activated: state.dashboard.activated,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchPosts: () => (dispatch(actions.dashboardFetchPosts())),
        onUpdatePagination: (activated) => (dispatch(actions.dashboardUpdatePagination(activated)))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Dashboard,axios))