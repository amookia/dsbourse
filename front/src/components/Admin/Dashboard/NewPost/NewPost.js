import React,{Component} from 'react'
import Wrapper from '../../../../hoc/Wrapper/Wrapper'
import classes from './NewPost.css'
import TextEditor from '../../../UI/TextEditor/TextEditor'
import {axios2} from '../../../../axios'
import withErrorHandler from '../../../../hoc/withErrorHandler/withErrorHandler'
import { connect } from 'react-redux'
import Spinner from '../../../UI/Spinner/Spinner'
import Input from '../../../UI/Input/Input'
import Nodal from '../../../UI/Modals/Modals'
import Modals from '../../../UI/Modals/Modals'
import Button from '../../../UI/Button/Button'

class NewPost extends Component{
    state= {
        headImage: null,
        uploadFiles: [],
        uploadedLinks: [],
        uploading: false,
        headLink: null,
        body: '',
        controls: {
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'موضوع'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 30, 
                },
                valid: false,
                touched: false
            },
            description: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'توضیح مختصر',
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 200, 
                },
                valid: false,
                touched: false
            }
        },
        showModal: false,
        loading: false,
        error: false,
        success: false,
        message: '',
    }
    
    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({controls: updatedControls});
    }

    onNewPostHandler = () =>{
        if(this.state.controls.title.valid === true && this.state.controls.description.valid === true && this.state.headImage){
            console.log(this.state.headImage)
            this.setState({loading: true, showModal: true})
            this.uploadHeadImageHandler(this.state.headImage)
        }else{
            this.setState({error: true,message: "ورودی نامعتبر",showModal: true})
        }
    }
    
    headImge = (e) =>{
        let filesArray = []
        const files = e.target.files
        for (let key in files){
            if(files[key].type){
                filesArray.push(files[key])
            }
        }            
        this.setState({headImage: filesArray})
    }

    uploadFile = (e) => {
        let filesArray = []
        const files = e.target.files
        for (let key in files){
            if(files[key].type){
                filesArray.push(files[key])
            }
        }            
        this.setState({uploadFiles: filesArray})
        this.uploadFilesHandler(filesArray)
    }

    createPost = () => {
        const data = {
            "head_image" : this.state.headLink,
            "title" : this.state.controls.title.value,
            "description" : this.state.controls.description.value,
            "body" : this.state.body,
        }
        console.log(data)

        const config = {
            headers: {
                'Authorization': this.props.xtoken,
            }
        };
        axios2.post("/api/adminisgod/post/create",data,config)
        .then(response =>{
            this.setState({message: response.data.message,success: true})
        })
        .catch(error =>{
            this.setState({message: error.response.data.message,error: false})
        })

    }

    uploadFilesHandler = (files) => {
        const url = 'https://dastyarbourse.com'
        this.setState({uploading: true})
        for (let key in files){
            const formData = new FormData();
            formData.append('file',files[key]);
            const config = {
                headers: {
                    'Authorization': this.props.xtoken,
                    'content-type': 'multipart/form-data'
                }
            };
            axios2.post("/api/adminisgod/upload",formData,config)
            .then(response =>{
                this.setState({uploading: false})
                this.setState({uploadedLinks : [...this.state.uploadedLinks,`${url}/media/blog/${response.data.filename}`]})
                console.log(this.state.uploadedLinks)
            })
            .catch(error =>{
                this.setState({uploading: false})
                console.log(error.response)
                this.setState({message: error.response.data.error,error: true})

            })
        }
    }
    uploadHeadImageHandler = (files) => {
        const formData = new FormData();
        formData.append('file',files[0]);
        const config = {
            headers: {
                'Authorization': this.props.xtoken,
                'content-type': 'multipart/form-data'
            }
        };
        axios2.post("/api/adminisgod/upload",formData,config)
        .then(response =>{
            this.setState({headLink: response.data.filename})
            this.createPost()
        })
        .catch(error =>{
            this.setState({message: error.response.data.error,error: true})
        })
    }
    handleContent = (content) => {
        this.setState({body: content})
    }

    copyLink = (event) =>{
        this.link.select();
        document.execCommand('copy');
    }

    render(){
        const fileUpload = [classes.fileUpload, classes.btn, classes.btnSuccess]
        const upload = [classes.upload, classes.up]
        let imageHead = ""
        let filesUpload = ""
        for (let key in this.state.uploadFiles){
            filesUpload += this.state.uploadFiles[key].name + " , "
        }
        for (let key in this.state.headImage){
            imageHead += this.state.headImage[key].name 
        }
        let link = []
        link = this.state.uploadedLinks.map(link =>(
            <div className={classes.linkGroup}>
                <input type="text" className={classes.formControl} value={link} ref={(link) => this.link = link} readOnly/>
                <div>
                    <div className={fileUpload.join(" ")}>
                        <div className={classes.copy} onClick={event => this.copyLink(event)}>کپی کن</div>
                    </div>
                </div>
            </div>
        ))
        let links = null
        if(link.length !== 0){
            links = (
                <div className={classes.linksContainer}>    
                    {link}
                </div>
            )
        }
        if(this.state.uploading){
            links = (
                <div className={classes.linksContainer}>    
                    {<Spinner/>}
                </div>
            )
        }
        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }
        const rawInputs = formElementsArray.map( formElement => (
            <Input
                key={formElement.id}
                className={classes.postInputs}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                isValid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );
        let modal = <Modals container= {classes.modalContainer} classes={classes.modal} content={
            <lottie-player src={"https://assets2.lottiefiles.com/packages/lf20_9unpvaft.json"}  background={"transparent"}  speed={"1"}  style={{width: "300px" ,height: "300px"}} autoplay></lottie-player>
        }/>
        if(this.state.error){
            modal = <Modals
            classes={classes.modal}
            container= {classes.modalContainer}
            content={
                (
                    <div className={classes.modalContainer}>
                        <lottie-player src={"https://assets10.lottiefiles.com/packages/lf20_WUEvZP.json"}  background={"transparent"}  speed={"1"}  style={{width: "120px" ,height: "120px"}} autoplay></lottie-player>
                        <h2>{this.state.message}</h2>
                        <Button buttonType={"signIn"} onClick={() => this.props.history.push("/")}>تایید</Button>
                    </div>
                )
            }
            />
        }
        if(this.state.success){
            modal = <Modals
            classes={classes.modal}
            container= {classes.modalContainer}
            content={
                (
                    <div className={classes.modalContainer}>
                        <lottie-player src={"https://assets4.lottiefiles.com/private_files/lf30_qXYuJE.json"}  background={"transparent"}  speed={"1"}  style={{width: "215px" ,height: "215px"}}  loop autoplay></lottie-player>
                        <h2>{this.state.message}</h2>
                        <Button buttonType={"signIn"} onClick={() => this.props.history.push("/")}>تایید</Button>
                    </div>
                )
            }
            />
        }
        return(
            <Wrapper className={classes.postBoxContainer}>
                {this.state.showModal ? modal : null}
                <div className={classes.postBox}>
                    <div className={classes.postHeader}>
                        <h1 className={classes.pageHead}>پست جدید</h1>
                    </div>
                    <form className={classes.NewPost}>
                        {rawInputs}
                        <div className={classes.inputGroup}>
                            <input type="text" className={classes.formControl} value={imageHead} readOnly/>
                            <div>
                                <div className={fileUpload.join(" ")}>
                                    <div className={classes.upl}>تصویر پست</div>
                                    <input type="file" onChange={e => this.headImge(e)} className={upload.join(" ")}/>
                                </div>
                            </div>
                        </div>
                        <div className={classes.inputGroup}>
                            <input type="text" className={classes.formControl} value={filesUpload} readOnly/>
                            <div>
                                <div className={fileUpload.join(" ")}>
                                    <div className={classes.upl}>اپلود فایل</div>
                                    <input type="file" onChange={e => this.uploadFile(e)} className={upload.join(" ")} multiple/>
                                </div>
                            </div>
                        </div>
                    </form>
                    {links}
                    <div className={classes.editor}>
                        <TextEditor handleContent={this.handleContent}/>
                    </div>
                    <button className={classes.controllerButtons} onClick={this.onNewPostHandler}>درج پست</button> 
                </div>
            </Wrapper>
        )
    }
}

const mapStateToProps = state =>{
    return{
        xtoken: state.adminAuth.xtoken,
    }
}

export default connect(mapStateToProps,null)(NewPost)