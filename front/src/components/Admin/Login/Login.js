import React, { Component } from 'react'
import { connect } from 'react-redux'
import classes from './Login.css'
import * as actions from '../../../store/actions/index'
import Button from '../../UI/Button/Button'
import image from '../../../assets/images/main/login.webp'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import Input from '../../UI/Input/Input'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import axios from '../.././../axios'
import Spinner from '../../UI/Spinner/Spinner'
import imageNotFound from '../../../assets/images/articles/imageNotFound.webp'
import padlock from '../../../assets/images/icons/padlock.png'
import user from '../../../assets/images/icons/user.png'
import moon from '../../../assets/images/main/moon.png'
import buildings from '../../../assets/images/main/buildings.png'
import lock from '../../../assets/images/main/lock.png'

class Login extends Component{
    state = {
        button : {
            signIn: {
                buttonType: "signIn",
                value : "ورود",
            }
        },
        controls : {
            userName: {
                elementType: "input",
                elementConfig: {
                    type: "text",
                    placeholder: "نام کاربری",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            },
            password: {
                elementType: "input",
                elementConfig: {
                    type: "password",
                    placeholder: "رمز عبور",
                },
                value: "",
                validation: {
                    required: true,
                },
                valid: false,
                touched: false,
            }
        }
    }
    
    checkValidity(value, rules) {
        let isValid = true

        if(!rules) {
            return true
        }

        if(rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if(rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }
        return isValid

    }

    buttonOnClickHandler(event, buttonName){
        event.preventDefault()
        if (buttonName == "signIn"){
            const userName = this.state.controls.userName.value
            const password = this.state.controls.password.value
            this.props.onAuth(userName, password, () => this.props.history.push('/'))
        }
    }

    inputChangedHandler( event, controlName ){
        const updatedControls = {
            ...this.state.controls,
            [controlName] : {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true,
            }
        }
        this.setState({controls: updatedControls})
    }

    render(){
        const buttonElementArray = []
        for(let key in this.state.button){
            buttonElementArray.push({
                id: key,
                config: this.state.button[key],
            })
        }
        const buttons = buttonElementArray.map(buttonElement =>(
            <Button
            key={buttonElement.id}
            buttonType={buttonElement.config.buttonType}
            onClick={(event) => this.buttonOnClickHandler(event, buttonElement.id)}>{buttonElement.config.value}</Button>
        )) 
        const formElementArray = []
        for (let key in this.state.controls){
            formElementArray.push({
                id: key,
                config: this.state.controls[key],
            })
        }
        const inputsImages = [padlock,user]
        const form = formElementArray.map((formElement,index) =>(
            <div className={classes.inputContainer}>
                <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                isValid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                className={classes.loginInputs}
                onChange={(event) => this.inputChangedHandler(event, formElement.id)}
                />
                <img className={classes.inputIcons} src={inputsImages[index]}/>
            </div>
        ))
        let show = <Spinner/>
        if(!this.props.loading){
            show = (
                <div className={classes.loginContainer}>
                    <form className={classes.loginForm}>
                        <img className={classes.lock} src={lock}/>
                        {form}
                        <div className={classes.formButtons}>
                            {buttons}
                        </div>
                    </form>
                </div>
            )
        }
        if (this.props.error){
            show = (
                <div className={classes.loginContainer}>
                    <div className={classes.loginItem}>
                        <form className={classes.loginForm}>
                            <img className={classes.lock} src={lock}/>
                            {form}
                            <p className={classes.error}>{this.props.error}</p>
                            <div className={classes.formButtons}>
                                {buttons}
                            </div>
                        </form>
                    </div>
                </div>
            )
        }
        if (this.props.token != null) {
            this.props.history.push('/')
        }
        return( 
            <Wrapper className={classes.loginBox}>
                <img className={classes.moon} src={moon}/>
                <img className={classes.buildings} src={buildings}/>
                {show}
            </Wrapper>           
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.adminAuth.loading,
        error: state.adminAuth.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (userName, password, calbk) => dispatch(actions.adminAuth(userName, password, calbk))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)( withErrorHandler(Login, axios))