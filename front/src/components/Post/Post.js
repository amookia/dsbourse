import React, { Component } from 'react'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import axios from '../../axios'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import classes from './Post.css'
import imageNotFound from '../../assets/images/articles/imageNotFound.webp'
import Parser from 'html-react-parser';

class Post extends Component{
    state = {
        title: "",
        body: "",
        description: "",
        head_image: "",
        dateTime: ""
    }
    
    onFetchPost(){
        axios.get(`/api/blog/post/${this.props.match.params.id}`)
            .then(response => {
                this.setState({
                    title: response.data.title,
                    body: response.data.body,
                    description: response.data.description,
                    head_image: response.data.head_image,
                    dateTime: response.data.date
                })
            })
            .catch(error => {
                console.log(error.response)
            })    
    }
    

    componentDidMount(){
        this.onFetchPost()
    }

    render(){
        let importanClasses = ['content', classes.post]
        return(
        <Wrapper className={classes.blogBox}>
            <div className={classes.blogContainer}>
                <div className={classes.blogTitle}>
                    <h2>{this.state.title}</h2>
                </div>
                <img className={classes.images} src={this.state.head_image} onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                <div className={classes.blogTitle}>
                    <h2>{this.state.title}</h2>
                </div>
                <p className={classes.description}>{this.state.description}</p>
                <div className={importanClasses.join(' ')}>
                    {Parser(this.state.body)}
                </div>
            </div>
        </Wrapper>
        )
    }
}

export default withErrorHandler(Post,axios)