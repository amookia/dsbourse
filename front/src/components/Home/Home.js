import React , { Component, useImperativeHandle } from 'react'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import introShape from '../../assets/images/main/introShape.png'
import topMobile from '../../assets/images/main/topMobile.png'
import topMobileLogo from '../../assets/images/main/topLogoMobile.png'
import classes from './Home.css'
import customerImg from '../../assets/images/main/customer.png'
import customerShape from '../../assets/images/main/customerShape.png'
import projectImg from '../../assets/images/main/projectsImg.png'
import footerShape from '../../assets/images/main/footerShape.png'
import Input from '../UI/Input/Input'
import contatUsInput from '../../assets/images/main/contatUsInput.png'
import mobileMain from '../../assets/images/main/homeMobile.png'
import midMobileTop from '../../assets/images/main/midMobileTop.png'
import projectsImgMobile from '../../assets/images/main/projectsImgMobile.png'
import mobileFooter from '../../assets/images/main/mobileFooter.png'
import Typing from 'react-typing-animation'

class Home extends Component{
    state = {
        width: window.innerWidth,
        controls: {
            title: {
                className: classes.emailArea,
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'ایمیل'
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 30,
                },
                valid: false,
                touched: false
            },
            description: {
                className: classes.messeageArea,
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'متن را وارد کنید...',
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 200,
                },
                valid: false,
                touched: false
            }
        }
    }

    componentWillMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    }
    
    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({controls: updatedControls});
    }
    
    render(){
        const { width } = this.state;
        const isMobile = width <= 626;
        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }
        const rawInputs = formElementsArray.map( formElement => (
        <Input
            key={formElement.id}
            className={formElement.config.className}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            isValid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            onChange={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );
        if (isMobile) {
            return (
                <div className={classes.introMobile}>
                    <div className={classes.topMobileContainer}>
                        <div className={classes.logoContainer}>
                            <img src={topMobileLogo}></img>
                        </div>
                        <img src={topMobile}/>
                    </div>
                    <div className={classes.mobileMain}>
                        <img src={mobileMain}/>
                    </div>
                    <div className={classes.midMobileTop}>
                        <img src={midMobileTop}/>
                        <h1 className={classes.midMobileTopTitle}>هدف ما کسب رضایت شماست</h1>
                        <div className={classes.midMobileTopText}>
                            <p>هدف ما ساده تر کردن پیچیدگی های معامله است</p>
                            <p>دستیاربورس فکر همه  چی رو کرده</p>
                            <p>نظرات شما تاثیر مستقیم در شکل گیری بهبود و ارتقا</p>
                            <p>دستیاربورس دارد</p>
                        </div>
                    </div>
                    <div className={classes.midMobilebottom}>
                        <img src={topMobile}/>
                        <h1 className={classes.midMobileBottomTitle}>پروژه های انجام شده</h1>
                    </div>
                    <div className={classes.projectsImgMobile}>
                        <img src={projectsImgMobile}/>
                    </div>
                    <div className={classes.mobileFooter}>
                        <img src={mobileFooter}/>
                    </div>
                    <div className={classes.contactUsInputMobile}>
                        <h1 className={classes.contactUsTitleMobile}>ارتباط با ما</h1>
                        <div className={classes.contactUsMobileContainer}>
                            {rawInputs}
                            <button className={classes.confirmButton}>ثبت</button>
                        </div>
                    </div>
                </div>
            );
          } else {
            return (
                <Wrapper>
                    <div className={classes.intro}>
                        <p className={classes.introTitle}>دستیاربورس</p>
                        <div className={classes.introTexts}>
                            <p className={classes.introText}>سامانه تحلیل تکنیکال بورس و فراربورس</p>
                            <p className={classes.introText}>سریع ، هوشمند ، مقرون به صرفه</p>
                        </div>
                        <div className={classes.introShape}>
                            <div className={classes.introShapeTexts}>
                                <div className={classes.introShapeText}>
                                    <p className={classes.introText}>هدف ما ساده تر کردن پیچیدگی های معامله است</p>
                                    <p className={classes.introText}>دستیاربورس فکر همه  چی رو کرده</p>
                                </div>
                            </div>
                            <img src={introShape}/>
                        </div>
                    </div>
                    <div className={classes.customer}>
                        <img className={classes.customerImg} src={customerImg}/>
                        <div className={classes.customerShape}>
                            <div className={classes.customerShapeFirstTexts}>
                                <div className={classes.customerShapeText}>
                                    <p className={classes.customerText}>نظرات شما تاثیر مستقیم در</p>
                                    <p className={classes.customerText}>شکل گیری بهبود و ارتقا دستیاربورس دارد</p>
                                </div>
                            </div>
                            <div className={classes.customerShapeTexts}>
                                <p className={classes.customerText}>هدف ما کسب رضایت شماست</p>
                            </div>
                            <img src={customerShape}/>
                        </div>
                    </div>
                    <div className={classes.projects}>
                        <img  src={projectImg}/>
                        <p className={classes.projectsTitle}>پروژه های انجام شده</p>
                    </div>
                    <div className={classes.contactUs}>
                        <div className={classes.contactUsContaier}>
                            <div className={classes.contactUsInputs}>
                                <h1 className={classes.contactUsTitle}>ارتباط با ما</h1>    
                                {/* <img className={classes.contactUsInputsImg} src={contatUsInput}/> */}
                                {rawInputs}
                                <button className={classes.confirmButton}>ثبت</button>
                            </div>
                        </div>
                        <img src={footerShape}/>
                    </div>
                </Wrapper>
            );
          }
    }
}

export default Home