import React, { Component } from 'react'
import * as actions from '../../store/actions/index'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import { connect } from 'react-redux'
import axios from '../../axios'
import Spinner from '../UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'
import Pagination from '../UI/Pagination/Pagination'
import classes from './Blog.css'
import imageNotFound from '../../assets/images/articles/imageNotFound.webp'
import LazyImage from '../UI/LazyImage/LazyImage'
import {Link} from 'react-router-dom'

class Blog extends Component {

    componentDidMount(){
        this.props.onFetchIndex()
    }

    render(){
        let posts = <Spinner/>
        let pagination = null
        
        const pageinationBack = () =>{
            if (this.props.activated > 1){
                this.props.onUpdatePagination(this.props.activated - 1)
                this.props.onFetchPosts(this.props.activated - 1)
            }
        }

        const pageinationForward = () =>{
            if(this.props.activated < this.props.totalPages){
                this.props.onUpdatePagination(this.props.activated + 1)
                this.props.onFetchPosts(this.props.activated + 1)
            }
        }

        const pageinationNumber = (page) =>{
            this.props.onUpdatePagination(page)
            this.props.onFetchPosts(page)
        }

        if (!this.props.loading){
            if (this.props.posts){
                let post = this.props.posts.map(el =>(
                    <article className={classes.articleItems} key={el.id}>
                        <Link to={`/Blog/Post/${el.id}`}><LazyImage style={{backgroundColor: '#AFAFAF'}} src={el.head_image} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/></Link>
                        <h2>{el.title}</h2>
                        <p>{el.description}</p>
                        <div className={classes.fadeout}></div>
                        <Link to={`/Blog/Post/${el.id}`} className={classes.articleLink}>ادامه مطلب</Link>
                    </article>
                ))
                
                posts = (
                    <div className={classes.articlesBox}>
                        {post}
                    </div>
                )
                
                pagination = <Pagination iterate={this.props.posts} activated={this.props.activated} totalPages={this.props.totalPages} back={pageinationBack} forward={pageinationForward} page={pageinationNumber}/>
            }
        }
        
        return(
            <Wrapper className={classes.articles}>
                {posts}
                {pagination}
            </Wrapper>
        )
    }
}

const mapStateToProps = state =>{
    return{
        posts: state.blogs.posts,
        loading: state.blogs.loading,
        error: state.blogs.error,
        totalPages: state.blogs.totalPages,
        activated: state.blogs.activated,
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onFetchPosts: (page) => (dispatch(actions.blogsFetchPosts(page))),
        onFetchIndex: () => (dispatch(actions.blogsFetchIndex())),
        onUpdatePagination: (activated) => (dispatch(actions.blogsUpdatePagination(activated)))   
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(Blog,axios))
