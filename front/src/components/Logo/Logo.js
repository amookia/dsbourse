import React from 'react'
import logo from '../../assets/images/main/logo.png'
import logoAdmin from '../../assets/images/main/AdminLogo.png'
import classes from './Logo.css'

const Logo = (props) => {
    const logoClasses = [classes.Logo,props.className]
    return (
        <div className={logoClasses.join(' ')}>
            <img src={props.type ===  'admin' ? logoAdmin : logo} alt="no image!"/>
        </div>   
    )
};

export default Logo;