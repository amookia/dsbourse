import React,{ Component } from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import {axios2} from '../../../axios'
import classes from './InfoArea.css'
import Spinner from '../../UI/Spinner/Spinner'

class InfoArea extends Component{
    state = {
        bourse : null,
        farabourse : null   
    }
    componentDidMount(){
        axios2.get("/api/wallstreet/market")
        .then(response =>{
            this.setState({bourse: response.data.bourse,farabourse: response.data.farabourse})
        })
        .catch(error =>{
            console.log(error.response.data)
        })
    }
    render(){
        let content = <Spinner/>
        let container = [classes.info, classes.area]
        if (this.state.bourse != null && this.state.farabourse != null){
            content = (<Wrapper className={classes.infoCardsContainer}>
            <div className={classes.infoCard}>
                <div className={classes.infoTitle}>بازار بورس</div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>شاخص کل:</div>
                    <div className={classes.valueContainer}>
                        {this.state.bourse.index}
                        <div className={classes.detail}>({this.state.bourse.index_change})</div>  
                    </div>
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>شاخص هم وزن:</div>
                    <div className={classes.valueContainer}>
                        {this.state.bourse.index_h}
                        <div className={classes.detail}>({this.state.bourse.index_h_change})</div>  
                    </div>
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>ارزش بازار:</div>
                    {this.state.bourse.market_value}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>تعداد معاملات:</div>
                    {this.state.bourse.trade_number}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>حجم معاملات:</div>
                    {this.state.bourse.trade_volume}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>ارزش معاملات:</div>
                    {this.state.bourse.trade_value}
                </div>
            </div>
            <div className={classes.infoCard}>
                <div className={classes.infoTitle}>بازار فرابورس</div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>شاخص کل:</div>
                    <div className={classes.valueContainer}>
                        {this.state.farabourse.index}
                        <div className={classes.detail}>({this.state.farabourse.index_change})</div>  
                    </div>
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>ارزش بازار:</div>
                    {this.state.farabourse.market_value}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>تعداد معاملات:</div>
                    {this.state.farabourse.trade_number}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>حجم معاملات:</div>
                    {this.state.farabourse.trade_volume}
                </div>
                <div className={classes.infoContainer}>
                    <div className={classes.property}>ارزش معاملات:</div>
                    {this.state.farabourse.trade_value}
                </div>
            </div>
        </Wrapper>)
        }
        return(
            <Wrapper className={container.join(' ')} ss-container>
                {content}
            </Wrapper>
        )
    }
}

export default InfoArea