import React ,{ Component } from 'react'
import Wrapper from '../../hoc/Wrapper/Wrapper'
import classes from './Tweet.css'
import InfoArea from './InfoArea/InfoArea'
import MessageArea from './messagArea/messageArea'
import Modals from '../UI/Modals/Modals'
import Input from '../UI/Input/Input'
import {axios2} from '../../axios'
import {connect} from 'react-redux'
import Spinner from '../UI/Spinner/Spinner'
import imageNotFound from '../../assets/images/articles/imageNotFound.webp'
import LazyImage from '../UI/LazyImage/LazyImage'
import Parser from 'html-react-parser'

class Wallstreet extends Component{
    state = {
        showModal: true,
        headImage: null,
        messege: null,
        messegeId: null, 
        controls: {
            description: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'توضیح مختصر',
                },
                value: '',
                validation: {
                    required: true,
                    maxLength: 900, 
                },
                valid: false,
                touched: false,
                loading: false,
            }
        },
        ModalContent: null,
        error: null,
        success: null,
    }
    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }
        
        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        return isValid;
    }
    handleContent = (content) => {
        this.setState({body: content})
    }
    onNewTweet = () =>{
        if(this.props.token || this.props.xtoken){
            this.setState({showModal: true,ModalContent: 'tweet'})
        }else{
            this.setState({showModal: true,error: 'برای ثبت توییت ابتدا باید عضو شوید'})
        }
    }
    onCommentHandler= (id) =>{
        axios2.get(`/api/wallstreet/tweet/${id}`)
        .then(response =>{
            console.log(response)
            this.setState({showModal: true,messege: response.data,ModalContent: 'comment',messegeId: id,loading: false})
        })
        .catch(error =>{
            this.setState({error: error.response.data.error,loading: false})
        })
    }
    onLikeHandler= (id,liked) =>{
        console.log(id,liked)
        if(this.props.token || this.props.xtoken){
            const config ={
                headers: {
                    'Authorization': this.props.token ? this.props.token : this.props.xtoken,
                }
            }
            const data = {
                'like' : !liked
            }
            axios2.post(`/api/wallstreet/like/${id}`,data,config)
            .then(response =>{
                console.log(response.data)
            })
            .catch(error =>{
                this.setState({showModal: true,error: error.response.data.error})
            })
        }else{
            this.setState({showModal: true,error: 'برای لایک پست ابتدا باید عضو شوید'})
        }
    }
    getBase64(file, cb) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }
    headImge = (e) =>{
        let filesArray = []
        const files = e.target.files
        for (let key in files){
            if(files[key].type){
                filesArray.push(files[key])
            }
        }            
        this.setState({headImage: filesArray})
    }
    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({controls: updatedControls});
    }
    onSendNewPost = (headImageBase64) =>{
        this.setState({loading: true})
        let data = {
            "text": this.state.controls.description.value,
        }
        if(headImageBase64 != null){
            data = {
                "text": this.state.controls.description.value,
                "image": [headImageBase64,]
            }
        }
        const config = {
            headers: {
                'Authorization': this.props.token ? this.props.token : this.props.xtoken,
            }
        };
        axios2.post("/api/wallstreet/tweet",data,config)
        .then(response =>{
            const updatedControls = {
                ...this.state.controls,
                ['description']:{
                    ...this.state.controls['description'],
                    value: '',
                }
            }
            this.setState({success: response.data.message,headImage: null,controls: updatedControls,loading: false})
        })
        .catch(error =>{
            this.setState({error: error.response.data.error,loading: false})
        })
    }
    onSendNewComment = (headImageBase64) =>{
        if(this.props.token || this.props.xtoken){
            this.setState({loading: true})
            let data = {
                "text": this.state.controls.description.value,
            }
            if(headImageBase64 != null){
                data = {
                    "text": this.state.controls.description.value,
                    "image": [headImageBase64,]
                }
            }
            const config = {
                headers: {
                    'Authorization': this.props.token ? this.props.token : this.props.xtoken,
                }
            };
            axios2.post(`/api/wallstreet/comment/${this.state.messegeId}`,data,config)
            .then(response =>{
                const updatedControls = {
                    ...this.state.controls,
                    ['description']:{
                        ...this.state.controls['description'],
                        value: '',
                    }
                }
                this.setState({success: response.data.message,headImage: null,controls: updatedControls,messegeId: null,loading: false})
            })
            .catch(error =>{
                this.setState({error: error.response.data.error,loading: false})
            })
        }else{
            this.setState({showModal: true,error: 'برای ثبت کامنت ابتدا باید عضو شوید'})
        }
    }
    onNewPost = () =>{
        let headImageBase64 = '';
        if(this.state.headImage != null){
            this.getBase64(this.state.headImage[0], (result) => {
                headImageBase64 = result;
                this.onSendNewPost(headImageBase64)
        });
        }else{
            this.onSendNewPost(null)
        }
    }
    onNewComment = () =>{
        let headImageBase64 = '';
        if(this.state.headImage != null){
            this.getBase64(this.state.headImage[0], (result) => {
                headImageBase64 = result;
                this.onSendNewComment(headImageBase64)
        });
        }else{
            this.onSendNewComment(null)
        }
    }
    render(){
        let imageHead = ""
        const fileUpload = [classes.fileUpload, classes.btn, classes.btnSuccess]
        const upload = [classes.upload, classes.up]
        const formElementsArray = [];
      
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }
        const rawInputs = formElementsArray.map( formElement => (
            <Input
                key={formElement.id}
                className={classes.editor}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                isValid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                onChange={( event ) => this.inputChangedHandler( event, formElement.id )} />
        ) );
        for (let key in this.state.headImage){
            imageHead += this.state.headImage[key].name 
        }
        let modal = null
        switch(this.state.ModalContent){
            case 'tweet':
                modal = <Modals
                classes={classes.modalTweet}
                onClick={() => {this.setState({showModal: false})}}
                content={(
                    <div className={classes.container}>
                        <h1>توییت جدید</h1>
                        <div className={classes.inputGroup}>
                            <input type="text" className={classes.formControl} value={imageHead} readOnly/>
                            <div>
                                <div className={fileUpload.join(" ")}>
                                    <div className={classes.upl}>تصویر پست</div>
                                    <input type="file" onChange={e => this.headImge(e)} className={upload.join(" ")}/>
                                </div>
                            </div>
                        </div>
                        {rawInputs}
                        <button className={classes.newPostButton} onClick={this.onNewPost}>ثبت</button>
                    </div>
                )}
                />
                break
            case 'comment':
                console.log(this.state.messege)
                let messegeImge = [classes.titleItem, classes.messegeImage]
                let comments = null
                if(this.state.messege.comments !== null){
                    comments = this.state.messege.comments.map((el,idx) =>(
                        <div className={classes.messageContainer} key={el.idx}>
                            <div className={classes.messageTitle}>
                                <LazyImage className={messegeImge.join(" ")} src={el.avatar} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                                {/* <p className={classes.titleItem}>مهدی</p> */}
                                <p className={classes.titleItem}>{`@${el.username}`}</p>
                            </div>
                            <div className={classes.messageText}>
                                {/* <p className={classes.tag}>#خساپا</p> */}
                                <p>{Parser(el.text.replace(/\n/g, '<br>\n'))}</p>
                                {el.images ? 
                                <div className={classes.imageWrap} >{
                                    el.images.map( (el,idx) =>(
                                    <LazyImage key={idx} src={el} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                                ))
                                }</div>
                                : null}
                            </div>
                        </div>
                    )) 
                }
                modal = <Modals
                classes={classes.modal}
                onClick={() => {this.setState({showModal: false})}}
                content={(
                    (   <div>
                            <div className={classes.container}>
                                <h1>کامنت</h1>
                                <div className={classes.messageContainer}>
                                    <div className={classes.messageTitle}>
                                        <LazyImage className={messegeImge.join(" ")} src={this.state.messege.user[0].avatar} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                                        <p className={classes.titleItem}>{`@${this.state.messege.user[0].username}`}</p>
                                    </div>
                                    <div className={classes.messageText}>
                                        <p>{Parser((this.state.messege.text.replace(/\n/g, '<br>\n')).replace(/#([^ ]+)/, `<span class=${classes.hashtag}>#$1</span>`))}</p>
                                        {this.state.messege.images ? 
                                        <div className={classes.imageWrap} >{
                                            this.state.messege.images.map( (el,idx) =>(
                                            <LazyImage key={idx} src={el} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                                        ))
                                        }</div>
                                        : null}
                                </div>
                            </div>
                            <div className={classes.commentContainer}>
                                <div className={classes.inputGroup}>
                                    <input type="text" className={classes.formControl} value={imageHead} readOnly/>
                                    <div>
                                        <div className={fileUpload.join(" ")}>
                                            <div className={classes.upl}>تصویر کامنت</div>
                                            <input type="file" onChange={e => this.headImge(e)} className={upload.join(" ")}/>
                                        </div>
                                    </div>
                                </div>
                                {rawInputs}
                                <button className={classes.newPostButton} onClick={this.onNewComment}>ثبت</button>
                            </div>
                            <div className={classes.container}>
                                <h1>نظرات</h1>
                                {comments}
                            </div>
                        </div>
                        </div>
                )
                )}
                />
                break
        }
        
        if(this.state.error != null){
            modal = <Modals
            classes={classes.modalError}
            onClick={() => {this.setState({showModal: false,error: null})}}
            content={(
                <div className={classes.container}>
                    <lottie-player src={"https://assets10.lottiefiles.com/packages/lf20_WUEvZP.json"}  background={"transparent"}  speed={"1"}  style={{width: "120px" ,height: "120px"}} autoplay></lottie-player>
                    <h1>خطا</h1>
                    <p>{this.state.error}</p>
                    <button className={classes.newPostButton} onClick={() => this.setState({showModal: false,success: null,headImage: null,error: null})}>اوکی</button>
                </div>
            )}
            />
        }
        if (this.state.success != null) {
            modal = <Modals
            classes={classes.modalSuccess}
            onClick={() => {this.setState({showModal: false,success: null})}}
            content={(
                <div className={classes.container}>
                    <lottie-player src={"https://assets4.lottiefiles.com/private_files/lf30_qXYuJE.json"}  background={"transparent"}  speed={"1"}  style={{width: "215px" ,height: "215px"}} autoplay></lottie-player>
                    <h1>باتشکر</h1>
                    <p>{this.state.success}</p>
                    <button className={classes.newPostButton} onClick={() => this.setState({showModal: false,success: null,headImage: null,error: null})}>اوکی</button>
                </div>
            )}
            />
        }
        if(this.state.loading){
            modal = <Modals
            classes={classes.modalSuccess}
            onClick={() => {this.setState({showModal: false,success: null})}}
            content={(
                <div className={classes.container}>
                    <lottie-player src={"https://assets10.lottiefiles.com/packages/lf20_d7raikbb.json"}  background="transparent"  speed="1"  style={{width: "300px", height: "300px"}}  loop autoplay></lottie-player>
                </div>
            )}
            />
        }
        return(
            <Wrapper className={classes.wallStreetBox}>
                {this.state.showModal ? modal : null}
                <MessageArea onNewTweet={this.onNewTweet} onCommentHandler={this.onCommentHandler} onLikeHandler={this.onLikeHandler} token={this.props.xtoken ? this.props.xtoken : this.props.token}/>
            </Wrapper>
        )
    }
}

const mapStateToProps = state =>{
    return{
        xtoken: state.adminAuth.xtoken,
        token: state.userAuth.token,
    }
}

export default connect(mapStateToProps)(Wallstreet)