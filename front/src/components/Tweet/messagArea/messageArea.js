import React, { Component } from 'react'
import Wrapper from '../../../hoc/Wrapper/Wrapper'
import classes from './messageArea.css'
import SearchBox from '../../UI/SearchBox/SearchBox'
import {axios2} from '../../../axios'
import Spinner from '../../../components/UI/Spinner/Spinner'
import imageNotFound from '../../../assets/images/articles/imageNotFound.webp'
import LazyImage from '../../UI/LazyImage/LazyImage'
import Parser from 'html-react-parser';
import profile from '../../../assets/images/icons/profile.png'
import comment from '../../../assets/images/icons/comment.png'
import tweet from '../../../assets/images/icons/twitter.png'
import noliked from '../../../assets/images/icons/noliked.png'
import likedImg from '../../../assets/images/icons/liked.png'


let isMounted = false
class MessageArea extends Component{
    state = {
        messeges: null,
        loading: false,
    }
    updateMessges(){
        setInterval(() => {
            if (isMounted && this.state.messeges !== null){
                let UpdatedTweets =  [...this.state.messeges.tweets]
                let config = {
                    headers : {
                        'Authorization' : this.props.token,
                    }
                }
                if (this.props.token === null) {
                    config = null
                }
                axios2.get("/api/wallstreet/index",config)
                .then(response =>{
                    console.log(response.data.tweets)
                    response.data.tweets.map(el =>{
                        if(this.state.messeges.tweets.some(e => e.id === el.id)){
                            console.log("nothing new")
                            this.state.messeges.tweets.map((el,idx) =>{
                                axios2(`/api/wallstreet/tweet/${el.id}`,config)
                                .then(response =>{
                                    UpdatedTweets[idx] = {
                                        'comments' : response.data.comments,
                                        'id': el.id,
                                        'text': response.data.text,
                                        'likes': response.data.likes,
                                        'liked' : response.data.liked,
                                        'images': response.data.images,
                                        'user': [{
                                            'avatar': response.data.user[0].avatar,
                                            'user_id': response.data.user[0].id,
                                            'username': response.data.user[0].username
                                        }]
                                    }
                                    console.log(response.data,UpdatedTweets)
                                    this.setState({messeges: {
                                        'tweets': UpdatedTweets
                                    }})
                                })
                                .catch(error =>{
                                    console.log(error)
                                })
                            })
                        }else{
                            const updatedMessges = {
                                'tweets' : [el,...this.state.messeges.tweets]
                            }
                            console.log(updatedMessges)
                            this.setState({messeges: updatedMessges})
                            console.log("found new!")
                        }
                    })
                })
                .catch(error =>{
                    console.log(error)
                })
            }
        }, 20 * 1000)
    }

    componentDidMount(){
        this.setState({loading: true})
        let config = {
            headers : {
                'Authorization' : this.props.token,
            }
        }
        if (this.props.token === null) {
            config = null
        }
        axios2.get("/api/wallstreet/index",config)
        .then(response =>{
                this.setState({messeges: response.data,loading: false})
                isMounted = true
                this.updateMessges()
        })
        .catch(error =>{
            console.log(error)
        })
      
    }

    componentWillUnmount(){
        isMounted = false
    }

    render(){
        let messegeImge = [classes.titleItem, classes.messegeImage]
        let content = <Spinner/>
        if(this.state.loading == false && this.state.messeges != null){
            content = this.state.messeges.tweets.map(el =>{
                let liked = el.liked
                let likesClasses = classes.notLiked
                if(liked === true){
                    likesClasses = classes.liked
                }
                return(
                <div className={classes.messageContainer}  key={el.id}>
                    <div className={classes.messageTitle} onClick={() => this.props.onCommentHandler(el.id)}>
                        {/* <p className={classes.titleItem}>مهدی</p> */}
                        <p className={classes.titleItem}>{`@${el.user[0].username}`}</p>
                        <LazyImage className={messegeImge.join(" ")} src={el.user[0].avatar} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                    </div>
                    <div className={classes.messageText} onClick={() => this.props.onCommentHandler(el.id)}>
                        <p>{Parser((el.text.replace(/\n/g, '<br>\n')).replace(/#([^ ]+)/, `<span class=${classes.hashtag}>#$1</span>`))}</p>
                            {el.images ? 
                            <div className={classes.imageWrap}>{
                                el.images.map( (el,idx) =>(
                                <LazyImage key={idx} src={el} alt=''  onError={(e) => (e.target.onerror = null, e.target.src = imageNotFound)}/>
                                ))}</div>
                            : null}
                        <div className={classes.fadeout}></div>
                    </div>
                    <div className={classes.tweetsTools}>
                        <img src={comment} onClick={() => this.props.onCommentHandler(el.id)}/>
                        <img src={el.liked ? likedImg : noliked} onClick={() => this.props.onLikeHandler(el.id,el.liked)}/>
                        <p style={{color: el.liked ? "#fe618c" : "#D6D7D9"}}>{el.likes != 0 ? el.likes : null}</p>
                    </div>
                </div> 
            )})
        }
        return(
            <Wrapper className={classes.message}>
                <div className={classes.titleContainer}>
                    <SearchBox/>
                </div> 
                <div className={classes.wallstreetMain}>
                    <div className={classes.ScrollMessagesContainer}>
                        {content}
                    </div>
                </div>
                <div className={classes.newTweet} onClick={this.props.onNewTweet}>
                    <img src={tweet} className={classes.controlButtons}/>
                </div>
            </Wrapper>
            
        )
    }
}



export default MessageArea