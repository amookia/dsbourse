import React from 'react'
import classes from './NavigationItems.css'
import NavigationItem from './NavigationItem/NavigationItem'
import Wrapper from '../../../hoc/Wrapper/Wrapper'

const NavigationItems = (props) => {
    let navigationitems = (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/Tweet">توییت</NavigationItem>
            <NavigationItem link="/Blog">بلاگ</NavigationItem>
            <NavigationItem link="/" exact>صفحه اصلی</NavigationItem>
        </ul>
    )
    if (props.isAdminAuth){
        navigationitems = (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/NewPost">پست جدید</NavigationItem>
            <NavigationItem link="/" exact>داشبورد</NavigationItem>
        </ul>
        )
    }
    if(props.isUserAuth){
        navigationitems = (
            <ul className={classes.NavigationItems}>
               <NavigationItem link="/Tweet">توییت</NavigationItem>
                <NavigationItem link="/Blog">بلاگ</NavigationItem>
                <NavigationItem link="/" exact>صفحه اصلی</NavigationItem>
            </ul>
            )
    }
    return(
    <ul className={classes.NavigationItems}>
        {navigationitems}
    </ul>
    )
}

export default NavigationItems