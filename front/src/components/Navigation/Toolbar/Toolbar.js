import React , { Component } from 'react'
import Logo from '../../Logo/Logo'
import NavigationItems from '../NavigationItems/NavigationItems'
import classes from './Toolbar.css'
import DrawerToggle from '../SideDrawer/DrawerToggle/DrawerToggle'
import Profile from "./Profile/Profile"
import topShape from "./../../../assets/images/main/topShape.png"

class Toolbar extends Component {
     render(){
        const navclasses = [classes.DesktopOnly,classes.itemsLogo]
        return (
            <header className={classes.Toolbar}>
                <div className={classes.ToolbarItems}>
                    <DrawerToggle clicked={this.props.drawerToggleClicked}/>
                    <div className={classes.DesktopOnly}>
                        <Profile 
                        isAdminAuth={this.props.isAdminAuth}
                        isUserAuth={this.props.isUserAuth}
                        onClick={this.props.signOut}
                        dropDown={this.props.dropDown}
                        onDropDown={this.props.onDropDown}
                        />
                    </div>
                    <div className={classes.topShape}>
                        <img src={topShape}/>
                    </div>
                    <nav className={navclasses.join(' ')}>
                        <NavigationItems 
                        loggedin={true}
                        isAdminAuth={this.props.isAdminAuth}
                        isUserAuth={this.props.isUserAuth}/>
                        <Logo/>
                    </nav>
                </div>
            </header>
        )
    }
}

{/* <header className={classes.Toolbar}>
<div className={classes.ToolbarItems}>
    <DrawerToggle clicked={this.props.drawerToggleClicked}/>
    <Logo className={classes.sideDrawerLogo}/>
    <div className={classes.DesktopOnly}>
        <Profile 
        isAdminAuth={this.props.isAdminAuth}
        isUserAuth={this.props.isUserAuth}
        onClick={this.props.signOut}
        dropDown={this.props.dropDown}
        onDropDown={this.props.onDropDown}
        />
    </div>
    <nav className={classes.DesktopOnly}>
        <NavigationItems 
        loggedin={true}
        isAdminAuth={this.props.isAdminAuth}
        isUserAuth={this.props.isUserAuth}/>
    </nav>
    <div className={classes.DesktopOnly}>
        <Logo/>
    </div>
</div>
</header> */}

export default Toolbar