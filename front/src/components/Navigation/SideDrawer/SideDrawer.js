import React, { Component } from 'react';
import { connect } from 'react-redux'
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.css';
import Backdrop from '../../UI/Backdrop/Backdrop';
import Wrapper from '../../../hoc/Wrapper/Wrapper';
import ADMIN_PROFILE_IMAGE from '../../../assets/images/Profiles/admin.webp'
import GUEST_USER_IMAGE from '../../../assets/images/Profiles/avatar.webp'
import SIDE_DRAWER from '../../../assets/images/main/sideDrawer.webp'
import Spinner from '../../UI/Spinner/Spinner'
import axios from '../../../axios'
import { Link } from 'react-router-dom'

class SideDrawer extends Component {
    state = {
        loggedin : true,
    }
    
    render(){
        let controlers = 
        (
            <ul className={classes.controlers}>
                <ul className={classes.controlers}>
                    <li  className={classes.controler}>
                        <Link to={"/Login"} >ورود</Link>
                    </li>
                </ul>
            </ul>
        )
        let content = (
            <div className={classes.profile} style={{background: `url(${SIDE_DRAWER})`,backgroundSize: 'cover',backgroundColor: '#647a8c'}}>
            <img className={classes.image} src={GUEST_USER_IMAGE}/>
            <div className={classes.infoContainer}>
                <p>{"کاربر مهمان"}</p>
            </div>
        </div>)
        if(this.props.isAdminAuth){
            controlers = 
            (
            <ul className={classes.controlers}>
                <ul className={classes.controlers}>
                    <li  className={classes.controler}>
                        <span onClick={this.props.signOut}>خروج</span>
                    </li>
                </ul>
            </ul>
            )
            content = (
                <div className={classes.profile} style={{background: `url(${SIDE_DRAWER})`,backgroundSize: 'cover',backgroundColor: '#647a8c'}}>
                <img className={classes.image} src={ADMIN_PROFILE_IMAGE}/>
                <div className={classes.infoContainer}>
                    <p className={classes.id}>{"@Admin"}</p>
                    <p>{"ادمین"}</p>
                </div>
            </div>)
        }
        else if(this.props.isUserAuth){
            controlers = 
            (
            <ul className={classes.controlers}>
                <ul className={classes.controlers}>
                    <li  className={classes.controler}>
                        <span onClick={this.props.signOut}>خروج</span>
                    </li>
                    <li  className={classes.controler}>
                        <Link to={"/Profile"} >تنظیمات</Link>
                    </li>
                </ul>
            </ul>
            )
            content = (
                <div className={classes.profile} style={{background: `url(${SIDE_DRAWER})`,backgroundSize: 'cover'}}>
                    <Link to="/Profile"><img className={classes.image} src={this.props.avatar}/></Link>
                    <div className={classes.infoContainer}>
                        <p className={classes.id}>{`@${this.props.userName}`}</p>
                        <p>{this.props.name}</p>
                    </div>
                </div>)
        }
        if(this.props.loading){
            content = <Spinner/>
        }
        
        let attachedClasses = [classes.SideDrawer, classes.Close];
        if (this.props.open) {
            attachedClasses = [classes.SideDrawer, classes.Open];
        }

        return (
            <Wrapper className={classes.Backdrop}>
                <Backdrop show={this.props.open} clicked={this.props.closed} />
                <div className={attachedClasses.join(' ')}>
                    {content}
                    <nav>
                        {controlers}
                        <NavigationItems 
                        loggedin={this.state.loggedin} 
                        isAdminAuth={this.props.isAdminAuth}
                        isUserAuth={this.props.isUserAuth}/>
                    </nav>
                </div>
            </Wrapper>  
        );
    }
};

const mapStateToProps = state => {
    return {
        avatar: state.profile.avatar,
        name: state.profile.name,
        phone: state.profile.phone,
        userName: state.profile.userName,
        loading: state.profile.loading,
        error: state.profile.error
    }
}

export default connect(mapStateToProps,null)(withErrorHandler(SideDrawer,axios));