import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux'
import { createStore, applyMiddleware, compose, combineReducers} from 'redux'
import thunk from 'redux-thunk'
import adminAuthReducer from './store/reducers/adminAuth'
import dashboardReducer from './store/reducers/dashboard'
import blogsReducer from './store/reducers/blogs'
import userAuthReducer from './store/reducers/userAuth'
import profileReducer from './store/reducers/profile'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
  adminAuth: adminAuthReducer,
  dashboard: dashboardReducer,
  blogs: blogsReducer,
  userAuth: userAuthReducer,
  profile: profileReducer,
})

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk)
))

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render( app, document.getElementById( 'root' ) );
