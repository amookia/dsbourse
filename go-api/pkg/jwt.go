package pkg

import (
	"dsbgo/conf"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

func JwtVerify(mytoken string) (bool, string) {
	hmacSampleSecret := []byte(conf.SECRET)
	jtoken, _ := jwt.Parse(mytoken, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSampleSecret, nil
	})

	if claim, ok := jtoken.Claims.(jwt.MapClaims); ok && jtoken.Valid {
		user := claim["identity"]
		return true, user.(string)
	} else {
		return false, ""
	}
}

func AdminJwtVerify(mytoken string) (bool, string) {
	hmacSampleSecret := []byte(conf.SECRET)
	jtoken, _ := jwt.Parse(mytoken, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
		return hmacSampleSecret, nil
	})

	if claim, ok := jtoken.Claims.(jwt.MapClaims); ok && jtoken.Valid {
		is_admin := claim["user_claims"]
		if is_admin == nil {
			return false, ""
		}
		admin := claim["identity"]
		return true, admin.(string)
	} else {
		return false, ""
	}
}

func AdminGenrateJWT(admin string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"identity":    admin,
		"user_claims": map[string]bool{"isadmin": true},
	})
	hmacSampleSecret := []byte(conf.SECRET)
	jwtstring, _ := token.SignedString(hmacSampleSecret)
	return jwtstring
}

func GenerateJWT(ph string) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"identity":    ph,
	})
	hmacSampleSecret := []byte(conf.SECRET)
	jwtstring, _ := token.SignedString(hmacSampleSecret)
	return jwtstring
}
