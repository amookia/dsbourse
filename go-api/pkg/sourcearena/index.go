package sourcearena

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type BourseST struct {
	Bourse struct {
		State               string `json:"state"`
		Index               string `json:"index"`
		IndexChange         string `json:"index_change"`
		IndexChangePercent  string `json:"index_change_percent"`
		IndexH              string `json:"index_h"`
		IndexHChange        string `json:"index_h_change"`
		IndexHChangePercent string `json:"index_h_change_percent"`
		MarketValue         string `json:"market_value"`
		TradeNumber         string `json:"trade_number"`
		TradeValue          string `json:"trade_value"`
		TradeVolume         string `json:"trade_volume"`
	} `json:"bourse"`
}

type FaraBourseST struct {
	FaraBourse struct {
		State              string `json:"state"`
		Index              string `json:"index"`
		IndexChange        string `json:"index_change"`
		IndexChangePercent string `json:"index_change_percent"`
		MarketValue        string `json:"market_value"`
		TradeNumber        string `json:"trade_number"`
		TradeValue         string `json:"trade_value"`
		TradeVolume        string `json:"trade_volume"`
	} `json:"fara-bourse"`
}

func GetAllStocks() (BourseST, FaraBourseST) {
	var bourse BourseST
	var fbourse FaraBourseST
	url_b := "https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_bourse"
	url_f := "https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_farabourse"
	resp_b,_ := http.Get(url_b)
	body_b,_ := ioutil.ReadAll(resp_b.Body)
	resp_f,_ := http.Get(url_f)
	body_f,_ := ioutil.ReadAll(resp_f.Body)
	json.Unmarshal(body_b,&bourse)
	json.Unmarshal(body_f,&fbourse)
	return bourse,fbourse
}