package images

import (
	"bytes"
	"dsbgo/conf"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
)

func microServiceConvert(image []byte) []byte {
	//image := bytes.NewReader(file)
	var buf = new(bytes.Buffer)
	writer := multipart.NewWriter(buf)
	im := bytes.NewReader(image)
	part, _ := writer.CreateFormFile("file", "dont care about name")
	io.Copy(part, im)

	writer.Close()
	url := fmt.Sprintf("http://%s/convert?type=webp", conf.IMAGINARY)
	req, _ := http.NewRequest("POST", url, buf)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := &http.Client{}
	res, _ := client.Do(req)
	defer res.Body.Close()
	imageBytes, _ := ioutil.ReadAll(res.Body)
	return imageBytes
}
