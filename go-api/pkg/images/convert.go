package images

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"regexp"
	"strings"

	"github.com/google/uuid"
)

const LIMIT_SIZE = 500 * 1000 //500 Kb
const BASE64_REGEX = `data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*`
const IMAGEDIR = "./media/wallstreet/"

func byteSizeLimit(image []byte) error {
	byte_size := len(image)
	if byte_size > LIMIT_SIZE {
		return errors.New("حجم عکس بیشتر از حد مجاز میباشد")
	}
	return nil
}

func base64toBytes(b64 string) ([]byte, error) {
	bytes, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		return []byte(""), errors.New("عکس نامعبتر میباشد")
	}
	return bytes, nil
}

func isValidImage(im []byte, format string) (string, error) {
	image_io := bytes.NewReader(im)
	switch format {
	case "png":
		_, err := png.Decode(image_io)
		if err == nil {
			return "png", nil
		}
	case "jpeg":
		_, err := jpeg.Decode(image_io)
		if err == nil {
			return "jpeg", nil
		}
	}
	return "", errors.New("فرمت عکس نامعبتر")
}

func trimBase64(b64 string) (string, string, error) {
	match, _ := regexp.MatchString(BASE64_REGEX, b64)
	if match {
		splited := strings.Split(b64, ",")
		b64_image := splited[1]
		switch splited[0] {
		case "data:image/png;base64":
			return b64_image, "png", nil
		case "data:image/jpeg;base64":
			return b64_image, "jpeg", nil
		case "data:image/jpg;base64":
			return b64_image, "jpeg", nil
		}
	}
	return "", "", errors.New("فرمت عکس نامعتبر")
}

func Convert(b64 string) (filename string, err error) {
	//trimming base64 string
	trimmed_b64, format, err := trimBase64(b64)
	if err != nil {
		return "", err
	}
	//convert base64 to byte
	image_byte, err := base64toBytes(trimmed_b64)
	if err != nil {
		return "", err
	}
	//checking for image size limitation
	if err = byteSizeLimit(image_byte); err != nil {
		return "", err
	}
	//image binary validation
	frmt, err := isValidImage(image_byte, format)
	if err != nil {
		return "", err
	}
	fmt.Println("image format : ", frmt)
	uuid, _ := uuid.NewUUID()
	newuuid := strings.Replace(uuid.String(), "-", "", 4)
	filename = newuuid + "." + "webp"
	editedImage := microServiceConvert(image_byte)
	fmt.Println(IMAGEDIR + filename)
	_ = ioutil.WriteFile(IMAGEDIR+filename, editedImage, 0644)
	return filename, nil
}
