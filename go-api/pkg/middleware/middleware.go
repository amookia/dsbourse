package middleware

import (
	"dsbgo/pkg"
	"github.com/gin-gonic/gin")


func IsAuthorized(c *gin.Context){
	token := c.GetHeader("Authorization") ; if len(token) != 0 {
		valid,_ := pkg.JwtVerify(token)
		if valid {
			c.Next()
		}else{
			c.AbortWithStatusJSON(403,gin.H{"error":"Token invalid"})
			return
		}
	}else{
		c.AbortWithStatusJSON(400,gin.H{"error":"jwt-token not found"})
		return
	}
}

func IsAdmin(c *gin.Context){
	token := c.GetHeader("Authorization") ; if len(token) != 0 {
		valid,_ := pkg.AdminJwtVerify(token)
		if valid {
			c.Next()
		}else{
			c.AbortWithStatusJSON(403,gin.H{"error":"Token invalid"})
			return
		}
	}else{
		c.AbortWithStatusJSON(400,gin.H{"error":"jwt-token not found"})
		return
	}
}