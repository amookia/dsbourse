package kavenegar

import (
	"dsbgo/conf"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/kavenegar/kavenegar-go"
)

func SendSMS(phone_number string) (otp string) {
	min := 100000
    max := 999999
	seed := rand.NewSource(time.Now().UnixNano())
	rnd := rand.New(seed)
	code := rnd.Intn(max - min + 1) + min
	otp = strconv.Itoa(code)
	go send(phone_number,otp)
	return otp
}

func send(phonenumber,otp string) {
	message := fmt.Sprintf("کد ورود دستیار بورس : %s\nمهلت استفاده یک دقیقه می باشد.",otp)
	api := kavenegar.New(conf.KAVENEGAR)
	sender := ""
	receptor := []string{phonenumber}
	if _, err := api.Message.Send(sender, receptor, message, nil); err != nil {
		switch err := err.(type) {
		case *kavenegar.APIError:
			fmt.Println(err.Error())
		case *kavenegar.HTTPError:
			fmt.Println(err.Error())
		default:
			fmt.Println(err.Error())
		}
	}
}
