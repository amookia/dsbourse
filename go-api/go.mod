module dsbgo

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/kavenegar/kavenegar-go v0.0.0-20200629080648-6e28263b7162
	github.com/thoas/go-funk v0.8.0
	github.com/yaa110/go-persian-calendar v0.6.1
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
