package conf

import "os"

var (
	MONGOURI    = os.Getenv("MONGOURI")
	DOMAIN      = "https://dastyarbourse.com"
	STATICMEDIA = "/media"
	SECRET      = os.Getenv("SECRET")
	KAVENEGAR   = os.Getenv("KAVENEGAR")
	IMAGINARY   = os.Getenv("IMAGINARY")
)
