package users

import (
	"dsbgo/internal/handler"
	"dsbgo/pkg/middleware"

	"github.com/gin-gonic/gin"
)


func UserRegister(app *gin.Engine){
	user := app.Group("/api/users")
	{
		user.POST("/register",handler.UserRegister)
		user.POST("/register/verify",handler.UserRegisterVerify)
		user.POST("/forget/s1",handler.UserForget1)
		user.POST("/forget/s2",handler.UserForget2)
		user.POST("/login",handler.UserLogin)
		user.GET("/me",middleware.IsAuthorized,handler.UserMe)
		user.PATCH("/editprofile",middleware.IsAuthorized,handler.UserEditProfile)
		user.POST("/editprofile/avatar",middleware.IsAuthorized,handler.UserEditAvatar)
	}
}