package wallstreet

import (
	"dsbgo/internal/handler"
	"dsbgo/pkg/middleware"

	"github.com/gin-gonic/gin"
)

func RegisterWallstreet(app *gin.Engine){
	wallstreet := app.Group("/api/wallstreet")
	{	
		wallstreet.POST("/tweet",middleware.IsAuthorized,handler.WallstreetTweetCreate)
		wallstreet.GET("/index",handler.WallstreetIndex)
		wallstreet.GET("/index/:page",handler.WallsreetTweetPagination)
		wallstreet.GET("/tweet/:id",handler.WallstreetVisitTweet)
		wallstreet.POST("/like/:id",middleware.IsAuthorized,handler.WallstreetLikeTweet)
		wallstreet.GET("/market",handler.WallstreetMarket)
		wallstreet.POST("/comment/:id",middleware.IsAuthorized,handler.WallstreetComment)
		wallstreet.POST("/search/tag",handler.WallstreetSearch)
	}
}
