package blog

import (
	"dsbgo/internal/handler"

	"github.com/gin-gonic/gin")


func RegisterBlog(app *gin.Engine){
	blog := app.Group("/api/blog")
	{
		blog.GET("/index",handler.BlogIndex)
		blog.GET("/page/:page",handler.BlogPaginate)
		blog.GET("/post/:id",handler.BlogVisitPost)
	}
}