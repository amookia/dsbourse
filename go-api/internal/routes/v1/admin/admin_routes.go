package admin

import (
	"dsbgo/internal/handler"
	"dsbgo/pkg/middleware"

	"github.com/gin-gonic/gin"
)

func RegisterAdmin(app *gin.Engine){
	admin := app.Group("/api/adminisgod")
	{
		admin.POST("/login",handler.AdminLogin)
		admin.POST("/changepw",middleware.IsAdmin,handler.AdminChangePass)
		admin.GET("/me",middleware.IsAdmin,handler.AdminMe)
		admin.POST("/post/create",middleware.IsAdmin,handler.AdminPostCreate)
		admin.DELETE("/post/:id",middleware.IsAdmin,handler.AdminPostDelete)
		admin.PATCH("/post/:id",middleware.IsAdmin,handler.AdminPostEdit)
		admin.POST("/newadmin",middleware.IsAdmin,handler.AdminNewAdmin)
		admin.POST("/upload",middleware.IsAdmin,handler.AdminUploadMedia)
	}
}