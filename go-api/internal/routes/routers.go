package routes

import (
	"dsbgo/pkg/images"
	"dsbgo/internal/routes/v1/admin"
	"dsbgo/internal/routes/v1/blog"
	"dsbgo/internal/routes/v1/users"
	"dsbgo/internal/routes/v1/wallstreet"

	"github.com/gin-gonic/gin"
)

func SetupRoutes(app *gin.Engine) {
	app.Static("/media/wallstreet/",images.IMAGEDIR) // Wallstreet Images
	app.Static("/media/blog/","./media/blog/")       // Blog Images
	app.Static("/media/avatar/","./media/avatar/")   // Avatar Images
	wallstreet.RegisterWallstreet(app) //  /api/wallstreet
	blog.RegisterBlog(app)			   //  /api/blog
	admin.RegisterAdmin(app)           //  /api/adminisgod
	users.UserRegister(app)            //  /api/users
}