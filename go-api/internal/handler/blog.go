package handler

import (
	"dsbgo/conf"
	"dsbgo/internal/models/blog"
	"fmt"
	//"time"

	"github.com/gin-gonic/gin"
	ptime "github.com/yaa110/go-persian-calendar"
)

func BlogIndex(c *gin.Context) {
	top := blog.GetBlogTop()
	top_resp := make([]blog.BlogIndexResp,len(top))
	for i, p := range top{
		top_resp[i].Description = p.Desc
		top_resp[i].HeadImage = conf.DOMAIN + "/media/blog/" + p.HeadImage
		top_resp[i].ID = p.ID.Hex()
		top_resp[i].Title = p.Title
	}
	x,count := blog.BlogPagination(1)
	resp := make([]blog.BlogIndexResp,len(x))
	for i, post := range x {
		resp[i].Description = post.Desc
		resp[i].HeadImage = conf.DOMAIN + "/media/blog/" + post.HeadImage
		resp[i].ID = post.ID.Hex()
		resp[i].Title = post.Title
	}
	c.JSON(200, gin.H{"top":top_resp,"posts": resp,"total":count})
}

func BlogPaginate(c *gin.Context) {
	var uri struct {Page int64 `uri:"page" binding:"required"`}
	err := c.ShouldBindUri(&uri) ; if err != nil {
		fmt.Println(err)
		c.JSON(400,gin.H{"error":"invalid number!"})
		return
	}
	x,_ := blog.BlogPagination(uri.Page)
	resp := make([]blog.BlogIndexResp,len(x))
	for i, post := range x {
		resp[i].Description = post.Desc
		resp[i].HeadImage = conf.DOMAIN + conf.STATICMEDIA + "/blog/" + post.HeadImage
		resp[i].ID = post.ID.Hex()
		resp[i].Title = post.Title
	}
	c.JSON(200,gin.H{"posts":resp})
}

func BlogVisitPost(c *gin.Context){
	var resp blog.BlogVisitResp
	id := c.Param("id")
	post := blog.GetBlogPost(id)
	if post.ID.IsZero() {
		c.AbortWithStatusJSON(404,gin.H{"error":"پست یافت نشد"})
		return
	}
	resp.Body = post.Body
	resp.Desc = post.Desc
	resp.HeadImage = conf.DOMAIN + conf.STATICMEDIA + "/blog/" + post.HeadImage
	resp.ID = post.ID.Hex()
	resp.Title = post.Title
	resp.Date = ptime.Unix(post.CreatedAt.Unix(),0,ptime.Iran()).Format("yyyy/MM/dd hh:mm")
	fmt.Println(resp.Date)
	c.JSON(200,resp)
	return
}
