package handler

import (
	"dsbgo/pkg"
	"dsbgo/internal/models"
	"dsbgo/internal/models/admin"
	"dsbgo/internal/models/blog"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/thoas/go-funk"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

func AdminLogin(c *gin.Context) {
	var form admin.Admin
	err := c.ShouldBind(&form)
	if err != nil {
		c.AbortWithStatusJSON(400, gin.H{"error": "invalid form"})
		return
	}
	//Search by username
	password := form.Password
	filter := bson.M{"username": form.Username}
	x := models.MongoDB.Collection("admin").FindOne(models.CTX, filter)
	x.Decode(&form)
	//Compare hash
	err = bcrypt.CompareHashAndPassword([]byte(form.Password), []byte(password))
	if err == nil {
		token := pkg.AdminGenrateJWT(form.Username)
		c.JSON(200, gin.H{"token": token})
		return
	} else {
		c.JSON(200, gin.H{"error": "یوزرنیم یا پسورد اشتباه میباشد"})
		return
	}
}

func AdminChangePass(c *gin.Context) {
	var form struct {
		NewPass string `json:"newpass" binding:"required,min=6,max=25`
		OldPass string `json:"oldpass" binding:"required,min=6,max=25`
	}
	var admin admin.Admin
	token := c.GetHeader("Authorization")
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "invalid form"})
		return
	}
	_, username := pkg.AdminJwtVerify(token)
	x := models.MongoDB.Collection("admin").FindOne(models.CTX, bson.M{"username": username})
	x.Decode(&admin)
	if form.NewPass == form.OldPass {
		c.AbortWithStatusJSON(200, gin.H{"error": "پسورد جدید و قدیم نباید یکسان باشد"})
		return
	}
	if err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(form.OldPass)); err == nil {
		hashed, _ := bcrypt.GenerateFromPassword([]byte(form.NewPass), bcrypt.MinCost)
		form.NewPass = string(hashed)
		filter := bson.M{"username": username}
		update := bson.M{"$set": bson.M{"password": form.NewPass}}
		models.MongoDB.Collection("admin").UpdateOne(models.CTX, filter, update)
		c.JSON(200, gin.H{"message": "پسورد با موفقیت تغییر یافت"})
		return
	} else {
		c.JSON(200, gin.H{"error": "پسورد اشتباه میباشد"})
		return
	}
}

func AdminMe(c *gin.Context) {
	token := c.GetHeader("Authorization")
	_, username := pkg.AdminJwtVerify(token)
	c.JSON(200, gin.H{"username": username})
}

func AdminPostCreate(c *gin.Context) {
	var form blog.Post
	token := c.GetHeader("Authorization")
	_, username := pkg.AdminJwtVerify(token)
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(200, gin.H{"error": "invalid form"})
		return
	}
	form.ID = primitive.NewObjectID()
	form.Author = username
	form.Count = 0
	form.CreatedAt = time.Now()
	models.MongoDB.Collection("posts").InsertOne(models.CTX, form)
	c.JSON(200, gin.H{"message": "پست با موفقیت ساخته شد", "id": form.ID.Hex()})
}

func AdminPostDelete(c *gin.Context) {
	id := c.Param("id")
	objid, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objid}
	models.MongoDB.Collection("posts").DeleteOne(models.CTX, filter)
	c.JSON(200, gin.H{"message": "پست با موفقیت حذف شد"})
}

func AdminNewAdmin(c *gin.Context) {
	var form, admin admin.Admin
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "invalid form"})
		return
	}
	filter := bson.M{"username": form.Username}
	x := models.MongoDB.Collection("admin").FindOne(models.CTX, filter)
	x.Decode(&admin)
	if admin.ID.IsZero() == false {
		c.AbortWithStatusJSON(200, gin.H{"error": "نام کاربری تکراری میباشد"})
		return
	}
	hashed, _ := bcrypt.GenerateFromPassword([]byte(form.Password), bcrypt.MinCost)
	form.Password = string(hashed)
	form.ID = primitive.NewObjectID()
	models.MongoDB.Collection("admin").InsertOne(models.CTX, form)
	c.JSON(200, gin.H{"message": "ادمین جدید با موفقیت ساخته شد"})
}

func AdminUploadMedia(c *gin.Context) {
	var formats = []string{"png", "jpeg", "jpg", "gif"}
	form, _ := c.FormFile("file")
	filename := form.Filename
	splited := strings.Split(filename, ".")
	format := strings.ToLower(splited[len(splited)-1])
	if funk.Contains(formats, format) {
		uid, _ := uuid.NewUUID()
		newuuid := strings.Replace(uid.String(), "-", "", 4)
		form.Filename = newuuid + "." + format
		c.SaveUploadedFile(form, "./media/blog/"+form.Filename)
		c.JSON(200, gin.H{"filename": form.Filename})
	} else {
		c.JSON(200, gin.H{"error": "فرمت فایل نامعتبر"})
	}
}

func AdminPostEdit(c *gin.Context) {
	var post struct {
		HeadImage string `bson:"head_image,omitempty" json:"head_image""`
		Desc      string `bson:"description,omitempty" json:"description"`
		Title     string `bson:"title,omitempty" json:"title"`
		Body      string `bson:"body,omitempty" json:"body"`
	}
	id := c.Param("id")
	_ = c.Bind(&post)
	objid, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objid}
	update := bson.M{"$set": post}
	_, err := models.MongoDB.Collection("posts").UpdateOne(models.CTX, filter, update)
	if err == nil {
		c.JSON(200, gin.H{"message": "پست با موفقیت تغییر یافت"})
		return
	} else {
		c.JSON(200, gin.H{"error": "تغییری ایجاد نشد"})
		return
	}
}
