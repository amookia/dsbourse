package handler

import (
	"dsbgo/conf"
	"dsbgo/pkg"
	"dsbgo/pkg/images"
	"dsbgo/pkg/sourcearena"
	"dsbgo/internal/models"
	"dsbgo/internal/models/forms"
	"dsbgo/internal/models/users"
	"dsbgo/internal/models/wallstreet"
	"fmt"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func WallstreetIndex(c *gin.Context) {
	var user_valid = false
	var user primitive.ObjectID
	token := c.GetHeader("Authorization")
	if len(token) != 0 {
		verify, user_idn := pkg.JwtVerify(token)
		if verify {
			user_valid = true
			user = users.UserFindPh(user_idn).ID
		}
	}
	fmt.Println(user_valid)
	tweets := wallstreet.GetAllTweets()
	resp := make([]wallstreet.RespTweet, len(tweets))
	for i, tweet := range tweets {
		useri := users.UserFind(tweet.UserID)
		resp[i].ID = tweet.ID
		resp[i].Text = tweet.Text
		resp[i].Likes = len(tweet.Likers)
		resp[i].User = append(resp[i].User, wallstreet.RespUser{
			tweet.UserID,
			useri.Username,
			conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + useri.Avatar,
		})
		//Images
		for _, image := range tweet.Images {
			image = conf.DOMAIN + conf.STATICMEDIA + "/wallstreet/" + image
			resp[i].Images = append(resp[i].Images, image)
		}
		//Comments
		for j, comment := range tweet.Comments {
			if j > 3 {
				break
			}
			user := users.UserFind(comment.UserID)
			resp[i].Comments = append(resp[i].Comments, wallstreet.RespTweetComment{
				comment.UserID,
				comment.Text,
				conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + user.Avatar,
				user.Username,
			})
		}
		//check if user liked the tweet
		if user_valid {
			if pkg.Contains(tweet.Likers, user.Hex()) {
				resp[i].Liked = true
			}
		}
	}
	c.JSON(200, gin.H{"tweets": resp})
}

func WallstreetVisitTweet(c *gin.Context) {
	var user_valid = false
	var user primitive.ObjectID
	token := c.GetHeader("Authorization")
	if len(token) != 0 {
		verify, user_idn := pkg.JwtVerify(token)
		if verify {
			user_valid = true
			user = users.UserFindPh(user_idn).ID
		}
	}
	fmt.Println(user_valid)
	var resp wallstreet.RespTweet
	id := c.Param("id")
	tweet := wallstreet.GetTweet(id)
	//Finding by id
	if len(tweet.Text) == 0 {
		c.JSON(404, gin.H{"error": "توییت یافت نشد"})
		return
	}
	//Tweet
	resp.ID = tweet.ID
	resp.Text = tweet.Text
	resp.Likes = len(tweet.Likers)
	//User
	useri := users.UserFind(tweet.UserID)
	resp.User = append(resp.User, wallstreet.RespUser{
		tweet.UserID,
		useri.Username,
		conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + useri.Avatar,
	})
	//Insert images
	for _, image := range tweet.Images {
		image = conf.DOMAIN + conf.STATICMEDIA + "/wallstreet/" + image
		resp.Images = append(resp.Images, image)
	}
	//Comments
	for _, comment := range tweet.Comments {
		user := users.UserFind(comment.UserID)
		resp.Comments = append(resp.Comments, wallstreet.RespTweetComment{
			comment.UserID,
			comment.Text,
			conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + user.Avatar,
			user.Username,
		})
	}
	//check if user liked the tweet
	if user_valid {
		if pkg.Contains(tweet.Likers, user.Hex()) {
			resp.Liked = true
		}
	}
	c.JSON(200, resp)
}

func WallstreetTweetCreate(c *gin.Context) {
	var form wallstreet.Tweet
	if err := c.ShouldBind(&form); err != nil {
		c.JSON(400, gin.H{"error": "invalid form!"})
		return
	}
	token := c.GetHeader("Authorization")
	_,ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	form.Writer = user.Username
	form.UserID = user.ID.Hex()
	fmt.Println(form.UserID)
	form.Likers = []string{}
	form.Comments = []wallstreet.TweetComment{}
	form.ID = primitive.NewObjectID()
	for i,b := range form.Images{
		filename,err := images.Convert(b)
		if err != nil {c.AbortWithStatusJSON(400,gin.H{"error":err.Error()});return}
		form.Images[i] = filename
	}
	models.MongoDB.Collection("wallstreet").InsertOne(models.CTX,&form)
	c.JSON(200, gin.H{"message": "توییت با موفقیت منتشر شد","id":form.ID.Hex()})
	return
}

func WallstreetLikeTweet(c *gin.Context) {
	var form forms.TweetLike
	token := c.GetHeader("Authorization")
	id := c.Param("id")
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "invalid form"})
		return
	}
	_, ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	liked := wallstreet.LikeTweet(id, user.ID.Hex(), form.Like)
	fmt.Println(liked)
	c.JSON(200, gin.H{"message": "Done"})
}

func WallstreetMarket(c *gin.Context) {
	bourse, fbourse := sourcearena.GetAllStocks()
	c.JSON(200, gin.H{"bourse": bourse.Bourse, "farabourse": fbourse.FaraBourse})
}

func WallstreetComment(c *gin.Context){
	id := c.Param("id")
	var form wallstreet.TweetComment
	err := c.ShouldBind(&form) ; if err != nil {
		c.JSON(400,gin.H{"error":"invalid form"})
		return
	}
	for i,b := range form.Images{
		filename,err := images.Convert(b)
		if err != nil {c.AbortWithStatusJSON(400,gin.H{"error":err.Error()});return}
		form.Images[i] = filename
	}
	token := c.GetHeader("Authorization")
	_,ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	form.UserID = user.ID.Hex()
	wallstreet.CommentTweet(form,id)
	c.JSON(200,gin.H{"message":"کامنت با موفقیت ارسال شد"})
}

func WallstreetSearch(c *gin.Context){
	var form forms.TweetSearch
	err := c.ShouldBindJSON(&form)
	if err != nil {
		c.JSON(400,gin.H{"error":"invalid form"})
		return
	}
	ret := wallstreet.SearchTweet(form.Tag)
	c.JSON(200,ret)
}

func WallsreetTweetPagination(c *gin.Context){
	var uri struct {Page int64 `uri:"page"`}
	c.ShouldBindUri(&uri)
	//-------------------
	var user_valid = false
	var user primitive.ObjectID
	token := c.GetHeader("Authorization")
	if len(token) != 0 {
		verify, user_idn := pkg.JwtVerify(token)
		if verify {
			user_valid = true
			user = users.UserFindPh(user_idn).ID
		}
	}
	fmt.Println(user_valid)
	tweets := wallstreet.TweetPagination(uri.Page)
	resp := make([]wallstreet.RespTweet, len(tweets))
	for i, tweet := range tweets {
		useri := users.UserFind(tweet.UserID)
		resp[i].ID = tweet.ID
		resp[i].Text = tweet.Text
		resp[i].Likes = len(tweet.Likers)
		resp[i].User = append(resp[i].User, wallstreet.RespUser{
			tweet.UserID,
			useri.Username,
			conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + useri.Avatar,
		})
		//Images
		for _, image := range tweet.Images {
			image = conf.DOMAIN + conf.STATICMEDIA + "/wallstreet/" + image
			resp[i].Images = append(resp[i].Images, image)
		}
		//Comments
		for j, comment := range tweet.Comments {
			if j > 3 {
				break
			}
			user := users.UserFind(comment.UserID)
			resp[i].Comments = append(resp[i].Comments, wallstreet.RespTweetComment{
				comment.UserID,
				comment.Text,
				conf.DOMAIN + conf.STATICMEDIA + "/avatar/" + user.Avatar,
				user.Username,
			})
		}
		//check if user liked the tweet
		if user_valid {
			if pkg.Contains(tweet.Likers, user.Hex()) {
				resp[i].Liked = true
			}
		}
	}
	c.JSON(200, gin.H{"tweets": resp})
}
