package handler

import (
	"dsbgo/conf"
	"dsbgo/pkg"
	"dsbgo/pkg/kavenegar"
	"dsbgo/internal/models"
	"dsbgo/internal/models/admin"
	"dsbgo/internal/models/forms"
	"dsbgo/internal/models/users"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/thoas/go-funk"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

func UserLogin(c *gin.Context) {
	// Just same input in login (no problem)
	// If u changed admin struct later,be carefull
	var form admin.Admin
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(200, gin.H{"error": "ورودی اشتباه میباشد"})
		return
	}
	password := form.Password
	filter := bson.M{"username": form.Username}
	x := models.MongoDB.Collection("users").FindOne(models.CTX, filter)
	x.Decode(&form)
	err = bcrypt.CompareHashAndPassword([]byte(form.Password), []byte(password))
	if err != nil {
		c.JSON(200, gin.H{"error": "نام کاربری یا رمز عبور اشتباه میباشد"})
		return
	}
	user := users.UserFind(form.ID.Hex())
	token := pkg.GenerateJWT(user.PhoneNumber)
	c.JSON(200, gin.H{"token": token})
	return
}

func UserMe(c *gin.Context) {
	token := c.GetHeader("Authorization")
	_, ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	user.Password = ""
	user.Avatar = conf.DOMAIN + "/media/avatar/" + user.Avatar
	c.JSON(200, user)
}

func UserEditProfile(c *gin.Context) {
	const rechex = "^[a-zA-Z0-9]+$"
	var form struct {
		Username string `bson:"username" json:"username,omitempty" binding:"min=3,max=10"`
		Name     string `bson:"name" json:"name,omitempty" binding:"min=2,max=50"`
	}
	token := c.GetHeader("Authorization")
	_, ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	err := c.Bind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "ورودی نامعتبر"})
		return
	}
	regex, _ := regexp.Compile(rechex)
	ismatch := regex.MatchString(form.Username)
	//Check if username is not duplicate
	if form.Username == user.Username || ismatch != true {
		c.JSON(200, gin.H{"error": "نام کاربری نامعتبر"})
		return
	}
	//check for duplicate username
	if len(form.Username) > 0 {
		filter := bson.M{"username": form.Username}
		count, _ := models.MongoDB.Collection("users").CountDocuments(models.CTX, filter)
		if count > 0 {
			c.JSON(200, gin.H{"error": "نام کاربری تکراری میباشد"})
			return
		}
	}
	//Update User Documents
	filter := bson.M{"phone_number": ph}
	update := bson.M{"$set": form}
	_, err = models.MongoDB.Collection("users").UpdateOne(models.CTX, filter, update)
	if err != nil {
		c.JSON(200, gin.H{"error": "تغییری ایجاد نشد"})
		return
	} else {
		c.JSON(200, gin.H{"message": "پروفایل با موفقیت ویرایش شد"})
		return
	}
}

func UserEditAvatar(c *gin.Context) {
	const SIZE = 400000
	token := c.GetHeader("Authorization")
	_, ph := pkg.JwtVerify(token)
	user := users.UserFindPh(ph)
	var formats = []string{"png", "jpeg", "jpg"}
	form, _ := c.FormFile("file")
	if form.Size > SIZE {
		c.JSON(400, gin.H{"error": "حجم فایل بیشتر از حد تعیین شده میباشد"})
		return
	}
	filename := form.Filename
	splited := strings.Split(filename, ".")
	format := strings.ToLower(splited[len(splited)-1])
	if funk.Contains(formats, format) {
		form.Filename = user.ID.Hex() + "." + format
		c.SaveUploadedFile(form, "./media/avatar/"+form.Filename)
		filter := bson.M{"phone_number": ph}
		update := bson.M{"$set":bson.M{"avatar": form.Filename}}
		go models.MongoDB.Collection("users").UpdateOne(models.CTX, filter, update)
		c.JSON(200, gin.H{"filename": form.Filename})
	} else {
		c.JSON(200, gin.H{"error": "فرمت فایل نامعتبر"})
	}
}

func UserRegister(c *gin.Context) {
	const rechex = "^[a-zA-Z0-9]+$"
	var form forms.UserRegister
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "ورودی نامعتبر"})
		return
	}
	//Chcek if phonenumber exists in (otp,users) collections
	phone_filter := bson.M{"phone_number": form.PhoneNumber}
	count_otp, _ := models.MongoDB.Collection("otp").CountDocuments(models.CTX, phone_filter)
	count_users, _ := models.MongoDB.Collection("users").CountDocuments(models.CTX, phone_filter)
	if count_users > 0 {
		c.JSON(200, gin.H{"error": "شماره تکراری میباشد"})
		return
	} else if count_otp > 0 {
		c.JSON(200, gin.H{"error": "شماره در مرحله ثبت نام میباشد"})
		return
	}
	//Check if username exists in (otp,users) collections
	username_filter := bson.M{"username": form.Username}
	count_otp, _ = models.MongoDB.Collection("otp").CountDocuments(models.CTX, username_filter)
	count_users, _ = models.MongoDB.Collection("users").CountDocuments(models.CTX, username_filter)
	if count_otp > 0 || count_users > 0 {
		c.JSON(200, gin.H{"error": "نام کاربری تکراری میباشد"})
		return
	}
	//Check username regex
	regex, _ := regexp.Compile(rechex)
	ismatch := regex.MatchString(form.Username)
	if ismatch != true {
		c.JSON(200, gin.H{"error": "فرمت نام کاربری اشتباه میباشد"})
		return
	}
	//Check phonenumber
	if form.PhoneNumber[:2] != "09" {
		c.JSON(200, gin.H{"error": "شماره اشتباه میباشد"})
		return
	}
	form.CreatedAt = time.Now()
	form.ID = primitive.NewObjectID()
	form.Code = kavenegar.SendSMS(form.PhoneNumber)
	go models.MongoDB.Collection("otp").InsertOne(models.CTX, form)
	c.JSON(200, gin.H{"message": "کد به شماره شما ارسال شد",
	"otp":form.Code,}) //!!!!!!!! ..... REMOVE OTP LATER ..... !!!!!!!!!
}

func UserRegisterVerify(c *gin.Context) {
	var form forms.UserVerify
	var user users.User
	var temp forms.UserRegister
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "ورودی نامعتبر"})
		return
	}
	//Check phonenumber
	if form.PhoneNumber[:2] != "09" {
		c.JSON(200, gin.H{"error": "شماره اشتباه میباشد"})
		return
	}
	//find user
	filter := bson.M{"phone_number": form.PhoneNumber}
	x := models.MongoDB.Collection("otp").FindOne(models.CTX, filter)
	x.Decode(&temp)
	if temp.ID.IsZero() {
		c.JSON(200, gin.H{"error": "شماره اشتباه"})
		return
	}
	if temp.Code != form.Code {
		c.JSON(200, gin.H{"error": "کد ورود اشتباه میباشد"})
		return
	}
	hashed, _ := bcrypt.GenerateFromPassword([]byte(temp.Password), bcrypt.MinCost)
	user.ID = primitive.NewObjectID()
	user.Username = temp.Username
	user.Name = temp.Name
	user.Avatar = "default.png"
	user.PhoneNumber = temp.PhoneNumber
	user.Password = string(hashed)
	go models.MongoDB.Collection("otp").DeleteOne(models.CTX, filter)
	go models.MongoDB.Collection("users").InsertOne(models.CTX, user)
	token := pkg.GenerateJWT(user.PhoneNumber)
	c.JSON(200, gin.H{"token": token})
}

func UserForget1(c *gin.Context) {
	var user users.User
	var form forms.UserForget1
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "ورودی نامعتبر"})
		return
	}
	filter := bson.M{"phone_number": form.PhoneNumber}
	x := models.MongoDB.Collection("users").FindOne(models.CTX, filter)
	x.Decode(&user)
	//Check if phonenumber exists
	if user.ID.IsZero() == false {
		form.Code = kavenegar.SendSMS(form.PhoneNumber)
		form.Forget = true
		form.CreatedAt = time.Now()
		form.ID = primitive.NewObjectID()
		go models.MongoDB.Collection("otp").InsertOne(models.CTX, form)
	}
	c.JSON(200, gin.H{"message": "کد به شماره شما ارسال شد"})
}

func UserForget2(c *gin.Context) {
	var user forms.UserForget1
	var form forms.UserForget2
	err := c.ShouldBind(&form)
	if err != nil {
		c.JSON(400, gin.H{"error": "ورودی نامعتبر"})
		return
	}
	filter := bson.M{"phone_number": form.PhoneNumber, "forget": true}
	x := models.MongoDB.Collection("otp").FindOne(models.CTX, filter)
	x.Decode(&user)
	if user.Code == form.Code {
		fmt.Println(form.Password)
		hashed, _ := bcrypt.GenerateFromPassword([]byte(form.Password), bcrypt.MinCost)
		form.Password = string(hashed)
		update := bson.M{"$set": bson.M{"password": form.Password}}
		filter_update := bson.M{"phone_number": form.PhoneNumber}
		filter_remove := bson.M{"phone_number": form.PhoneNumber, "forget": true}
		go models.MongoDB.Collection("otp").DeleteOne(models.CTX, filter_remove)
		go models.MongoDB.Collection("users").UpdateOne(models.CTX, filter_update, update)
		c.JSON(200, gin.H{"message": "رمز عبور با موفقیت تغییر یافت"})
		return
	}
	c.JSON(200, gin.H{"message": "کد اشتباه می باشد"})
}
