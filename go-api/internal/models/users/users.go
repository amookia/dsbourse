package users

import (
	"dsbgo/internal/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func UserFind(id string) User {
	var user User
	objectid,_ := primitive.ObjectIDFromHex(id) //Error is not handled :D ! BK
	filter := bson.M{"_id":objectid}
	finduser := models.MongoDB.Collection("users").FindOne(models.CTX,filter)
	_ = finduser.Decode(&user)
	return user
}


func UserFindPh(phone_number string) User{
	var user User
	filter := bson.M{"phone_number":phone_number}
	finduser := models.MongoDB.Collection("users").FindOne(models.CTX,filter)
	_ = finduser.Decode(&user)
	return user
}