package blog

type BlogIndexResp struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	HeadImage   string `json:"head_image"`
	Title       string `json:"title"`
}

type BlogVisitResp struct {
	ID        string `json:"id"`
	HeadImage string `json:"head_image"`
	Desc      string `json:"description"`
	Title     string `json:"title"`
	Body      string `json:"body"`
	Date      string `json:"date"`
}
