package blog

import (
	"dsbgo/internal/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func BlogPagination(page int64) ([]Post,int) {
	var frompost int64 = ( page - 1 ) * 8
    var topost   int64 = page * 8
	var count int
	options := options.Find()
	options.SetSort(bson.D{{"_id", -1}})
	options.SetSkip(frompost)
	options.SetLimit(topost)
	posts,_ := models.MongoDB.Collection("posts").Find(models.CTX,bson.M{},options)
	var postList []Post
	for posts.Next(models.CTX) {
		var post Post
		posts.Decode(&post)
		postList = append(postList, post)
	}
	count = len(postList)
	return postList,count
}

func GetBlogPost(id string) (Post){
	var post Post
	objid,_ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id":objid}
	update := bson.M{"$inc":bson.M{"count":1}}
	x := models.MongoDB.Collection("posts").FindOne(models.CTX,filter)
	x.Decode(&post)
	if post.ID.IsZero() == false {
		// goroutine has been implemented for faster response
	 	go models.MongoDB.Collection("posts").UpdateOne(models.CTX,filter,update)
	}
	return post
}

func GetBlogTop() ([]Post){
	var postList []Post
	options := options.Find()
	options.SetSort(bson.M{"count":-1})
	options.SetLimit(3)
	posts,_ := models.MongoDB.Collection("posts").Find(models.CTX,bson.M{},options)
	for posts.Next(models.CTX) {
		var post Post
		posts.Decode(&post)
		postList = append(postList, post)
	}
	return postList
}
