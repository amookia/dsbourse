package blog

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Post struct {
	ID        primitive.ObjectID `bson:"_id"`
	HeadImage string             `bson:"head_image" json:"head_image" binding:"required"`
	Desc      string             `bson:"description" json:"description" binding:"required"`
	Title     string             `bson:"title" json:"title" binding:"required"`
	Body      string             `bson:"body" json:"body" binding:"required"`
	Count 	  int                `bson:"count"`
	Author    string             `bson:"author"`
	CreatedAt time.Time          `bson:"created_at"`
}
