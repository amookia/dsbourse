package models

import (
	"context"
	"dsbgo/conf"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MongoDB *mongo.Database
var CTX = context.TODO()

func init() {
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:27017", conf.MONGOURI))
	client, err := mongo.Connect(CTX, clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Ping(CTX, nil)
	if err != nil {
		log.Fatal(err)
	}
	MongoDB = client.Database("dsbourse")
	//creating index for ttl (expire) otp collection
	model := mongo.IndexModel{
		Keys:    bson.M{"createdAt": 1},
		Options: options.Index().SetExpireAfterSeconds(60),
	}
	MongoDB.Collection("otp").Indexes().CreateOne(CTX, model)
}
