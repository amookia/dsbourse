package admin

import "go.mongodb.org/mongo-driver/bson/primitive"

type Admin struct {
	ID       primitive.ObjectID `bson:"_id"`
	Username string             `bson:"username" binding:"required,min=3,max=10`
	Password string             `bson:"password" binding:"required,min=6,max=50"`
}
