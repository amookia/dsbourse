package wallstreet

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)


type TweetComment struct {
	UserID 	    string         `bson:"user_id"`
	Text        string         `bson:"text" json:"text" binding:"required,max=255"`
	Images      []string 	   `bson:"images" json:"images" binding:"max=2"`
}

type Tweet struct {
	ID          primitive.ObjectID     `bson:"_id"`
	Text        string                 `bson:"text" json:"text" binding:"required,min=5,max=250"`
	Writer      string                 `bson:"writer"`
	UserID	    string                 `bson:"user_id"`
	Images      []string               `bson:"images" json:"images" binding:"max=2"`
	Likers 		[]string               `bson:"likers"`
	Comments    []TweetComment
	CreatedAt   time.Time              `bson:"created_at"`
}