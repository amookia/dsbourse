package wallstreet

import (
	"dsbgo/internal/models"

	"github.com/thoas/go-funk"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func GetAllTweets() []Tweet{
	options := options.Find()
	options.SetSort(bson.D{{"_id", -1}})
	options.SetLimit(5)
	filter := bson.D{{}}
	x,_ := models.MongoDB.Collection("wallstreet").Find(models.CTX,filter,options)
	var tweetList []Tweet
	for x.Next(models.CTX) {
		var tweet Tweet
		x.Decode(&tweet)
		tweetList = append(tweetList, tweet)
	}
	x.Close(models.CTX)
	return tweetList
}



func GetTweet(id string) Tweet{
	mongo_id,_ := primitive.ObjectIDFromHex(id)
	var tweet Tweet
	x := models.MongoDB.Collection("wallstreet").FindOne(models.CTX,bson.M{"_id":mongo_id})
	x.Decode(&tweet)
	return tweet

}


func LikeTweet(tweetid,userid string,like bool) (bool) {
	var tweet Tweet
	tweetobj,_ := primitive.ObjectIDFromHex(tweetid)
	x := models.MongoDB.Collection("wallstreet").FindOne(models.CTX,bson.M{"_id":tweetobj})
	x.Decode(&tweet)
	if tweet.ID.IsZero() {
		return false
	}else{
		if like == false && funk.Contains(tweet.Likers,userid) {
			filters := bson.M{"_id":tweetobj}
			updates := bson.M{"$pull":bson.M{"likers":userid}}
			models.MongoDB.Collection("wallstreet").UpdateOne(models.CTX,filters,updates)
		}else if like == true && funk.Contains(tweet.Likers,userid) != true{
			filters := bson.M{"_id":tweetobj}
			updates := bson.M{"$push":bson.M{"likers":userid}}
			models.MongoDB.Collection("wallstreet").UpdateOne(models.CTX,filters,updates)
		}
		return true
	}
}

func CommentTweet(comment TweetComment,postid string){
	objid,_ := primitive.ObjectIDFromHex(postid)
	filter := bson.M{"_id":objid}
	updates := bson.M{"$push":bson.M{"comments":comment}}
	models.MongoDB.Collection("wallstreet").UpdateOne(models.CTX,filter,updates)
}

func SearchTweet(tag string) []RespSearch {
	options := options.Find()
	options.SetSort(bson.D{{"_id", -1}})
	options.SetLimit(10)
	filter := bson.M{"text": bson.M{
		"$regex": "#" + tag,
	}}
	x,_ := models.MongoDB.Collection("wallstreet").Find(models.CTX,filter,options)
	var tweetList []RespSearch
	for x.Next(models.CTX) {
		var tweet RespSearch
		x.Decode(&tweet)
		tweetList = append(tweetList, tweet)
	}
	x.Close(models.CTX)
	return tweetList
}

func TweetPagination(page int64) []Tweet {
	var from int64 = ( page - 1 ) * 5
    var to   int64 = page * 5
	options := options.Find()
	options.SetSort(bson.D{{"_id", -1}})
	options.SetSkip(from)
	options.SetLimit(to)
	filter := bson.D{{}}
	x,_ := models.MongoDB.Collection("wallstreet").Find(models.CTX,filter,options)
	var tweetList []Tweet
	for x.Next(models.CTX) {
		var tweet Tweet
		x.Decode(&tweet)
		tweetList = append(tweetList, tweet)
	}
	x.Close(models.CTX)
	return tweetList
}

