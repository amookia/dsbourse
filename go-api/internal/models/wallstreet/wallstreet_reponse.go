package wallstreet

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type RespTweetComment struct {
	UserID 	    string         `bson:"user_id" json:"user_id"`
	Text        string         `bson:"text" json:"text"`
	Avatar 		string         `json:"avatar"`
	Username 	string         `json:"username"`
}

type RespUser struct {
	UserID  	string 		   `json:"user_id"`
	Username 	string         `json:"username"`
	Avatar      string         `json:"avatar"`
}

type RespTweet struct {
	ID          primitive.ObjectID     `bson:"_id" json:"id"`
	Text        string                 `bson:"text" json:"text"`
	Images      []string               `bson:"images" json:"images"`
	Likes		int					   `json:"likes"`
	User 		[]RespUser             `json:"user"`
	Liked 		bool				   `json:"liked"`
	Comments    []RespTweetComment     `json:"comments" json:"comments"`
}


type RespSearch struct {
	ID          primitive.ObjectID     `bson:"_id"  json:"id"`
	Text        string                 `bson:"text" json:"text"`
}