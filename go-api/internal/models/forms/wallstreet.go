package forms

//import "time"

// type TweetCreate struct {
// 	Text       string    `json:"text" binding:"required,min=5,max=250" bson:"text"`
// 	Image      []string  `json:"image" binding:"required,min=1,max=2" bson:"images"`
// 	Created_at time.Time `bson:"time"`
// }

type TweetLike struct {
	Like bool `json:"like"`
}

type TweetSearch struct {
	Tag string `json:"tag" binding:"required,max=12"`
}
