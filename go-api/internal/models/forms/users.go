package forms

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserRegister struct {
	ID          primitive.ObjectID `bson:"_id"`
	Username    string             `bson:"username" json:"username" binding:"required,min=3,max=20`
	Password    string             `bson:"password" json:"password" binding:"required,min=8,max=30`
	Name        string             `bson:"name" json:"name" binding:"required,min=2,max=30"`
	PhoneNumber string             `bson:"phone_number" json:"phone_number" binding:"required,numeric,max=11,min=11"`
	Code        string             `bson:"code"`
	CreatedAt   time.Time          `bson:"createdAt"`
}

type UserVerify struct {
	PhoneNumber string `bson:"phone_number" json:"phone_number" binding:"required,numeric,max=11,min=11"`
	Code        string `bson:"code" json:"code" binding:"required,numeric,max=6,min=6"`
}

type UserForget1 struct {
	ID          primitive.ObjectID `bson:"_id"`
	PhoneNumber string             `bson:"phone_number" json:"phone_number" binding:"required,numeric,max=11,min=11"`
	Code        string             `bson:"code"`
	Forget      bool               `bson:"forget"`
	CreatedAt   time.Time          `bson:"createdAt"`
}

type UserForget2 struct {
	ID          primitive.ObjectID `bson:"_id"`
	PhoneNumber string             `bson:"phone_number" json:"phone_number" binding:"required,numeric,max=11,min=11"`
	Password    string             `bson:"password" json:"new_password" binding:"required,min=8,max=30`
	Code        string             `bson:"code"`
}
