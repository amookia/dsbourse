### BLOG
- [x] /api/blog
  - [x] INDEX
  - [x] VISIT POST
  - [x] PAGINATION
  - [x] VISIT IMAGE

### WALLSTREET
- [x] /api/wallstreet
  - [x] CREATE TWEET
  - [x] VISIT TWEET
  - [x] LIKE TWEET
  - [x] COMMENT TWEET
  - [x] INDEX
  - [x] MARKET INDEX
  - [x] STATIC FILES
  - [ ] SEARCH TWEET

### ADMIN
- [x] /api/adminisgod
  - [x] ADMIN LOGIN
  - [x] CHANGE PASSWORD
  - [x] UPLOAD MEDIA
  - [x] STATIC FILES
  - [x] CREATE POST
  - [x] DELETE POST
  - [x] EDIT POST
  - [x] CREATE ADMIN
  - [x] GET ME
  - [x] GENERATE TOKEN

### USERS
- [x] /api/users
  - [x] SETUP REGEX FILTER USERNAME FOR EDIT PROFILE
  - [x] REGISTER
  - [x] REGISTER VERIFY
  - [x] FORGET PASS 1
  - [x] FORGET PASS 2
  - [x] LOGIN
  - [x] GET ME
  - [x] EDIT PROFILE
  - [x] EDIT PROFILE IMAGE
